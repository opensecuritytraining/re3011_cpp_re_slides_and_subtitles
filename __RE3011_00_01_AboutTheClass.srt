1
00:00:00,320 --> 00:00:02,000
Hello!

2
00:00:01,280 --> 00:00:04,160
And

3
00:00:02,000 --> 00:00:06,000
this class will cover

4
00:00:04,160 --> 00:00:09,519
reverse engineering of C++

5
00:00:06,000 --> 00:00:12,400
binaries. The video will discuss

6
00:00:09,519 --> 00:00:15,120
the topics of the class and also a

7
00:00:12,400 --> 00:00:17,199
little bit about the motivation behind

8
00:00:15,120 --> 00:00:20,000
the class, and what you would expect to

9
00:00:17,199 --> 00:00:23,279
see during this class. So regarding the

10
00:00:20,000 --> 00:00:26,000
topics of this class, we will discuss

11
00:00:23,279 --> 00:00:28,000
objects and both how to

12
00:00:26,000 --> 00:00:32,160
understand and that what you are looking

13
00:00:28,000 --> 00:00:34,719
at is actually a constructor how to

14
00:00:32,160 --> 00:00:37,760
understand from the constructor about

15
00:00:34,719 --> 00:00:41,280
the different objects. And also how to

16
00:00:37,760 --> 00:00:44,559
define the objects that you found in IDA,

17
00:00:41,280 --> 00:00:46,640
and how to handle all of those things.

18
00:00:44,559 --> 00:00:48,800
So this is the first topic that we will

19
00:00:46,640 --> 00:00:50,640
cover during this class.

20
00:00:48,800 --> 00:00:53,680
We will also cover

21
00:00:50,640 --> 00:00:55,600
inheritance, both like regular, basic

22
00:00:53,680 --> 00:00:58,480
inheritance but also multiple

23
00:00:55,600 --> 00:01:01,199
inheritance. So both of those topics will

24
00:00:58,480 --> 00:01:03,920
be discussed during this class, and also

25
00:01:01,199 --> 00:01:06,320
we will discuss how to of course

26
00:01:03,920 --> 00:01:09,840
define all of those things in IDA.

27
00:01:06,320 --> 00:01:11,280
And what is the best way to do those

28
00:01:09,840 --> 00:01:12,640
things when you are doing reverse

29
00:01:11,280 --> 00:01:15,439
engineering.

30
00:01:12,640 --> 00:01:16,560
And the last topic would be a basic

31
00:01:15,439 --> 00:01:17,360
Templates.

32
00:01:16,560 --> 00:01:20,479


33
00:01:17,360 --> 00:01:22,640
Because and this is also something

34
00:01:20,479 --> 00:01:25,200
that you would see a lot in binaries and

35
00:01:22,640 --> 00:01:28,000
it is important to understand how to

36
00:01:25,200 --> 00:01:29,920
handle templates, how to understand one

37
00:01:28,000 --> 00:01:32,720
of the functions that you are looking at

38
00:01:29,920 --> 00:01:34,560
is actually a template and so on.

39
00:01:32,720 --> 00:01:36,960
So, I would add

40
00:01:34,560 --> 00:01:39,040
that because we have limited time, we

41
00:01:36,960 --> 00:01:40,880
can't have all the topics that are

42
00:01:39,040 --> 00:01:42,640
relevant for reverse engineering of C++

43
00:01:40,880 --> 00:01:45,920
binaries.

44
00:01:42,640 --> 00:01:48,479
But we do have here the most fundamental

45
00:01:45,920 --> 00:01:50,320
parts and that I think will be very

46
00:01:48,479 --> 00:01:52,880
helpful and relevant for you when you're

47
00:01:50,320 --> 00:01:56,000
doing reverse engineering.

48
00:01:52,880 --> 00:01:59,680
I would add that, if you feel

49
00:01:56,000 --> 00:02:02,719
that the topic of

50
00:01:59,680 --> 00:02:04,880
x86 

51
00:02:02,719 --> 00:02:07,439
assembly is not something that you are

52
00:02:04,880 --> 00:02:10,479
familiar enough with, or if you feel you

53
00:02:07,439 --> 00:02:12,560
want to do a little more and

54
00:02:10,479 --> 00:02:15,040
practice in this topic so there are

55
00:02:12,560 --> 00:02:16,480
great courses and classes you can take

56
00:02:15,040 --> 00:02:19,680
in OST-2.

57
00:02:16,480 --> 00:02:22,080
And this class will assume that the

58
00:02:19,680 --> 00:02:25,520
basic knowledge that you have is that

59
00:02:22,080 --> 00:02:28,400
you do have know how to do reversing

60
00:02:25,520 --> 00:02:30,560
of binaries after basic binaries, and

61
00:02:28,400 --> 00:02:32,640
that you do understand how to read the

62
00:02:30,560 --> 00:02:35,680
x64

63
00:02:32,640 --> 00:02:37,280
architecture. So, this is something that

64
00:02:35,680 --> 00:02:39,280
is important to

65
00:02:37,280 --> 00:02:42,000
note before we start.

66
00:02:39,280 --> 00:02:44,560
Another thing that i wanted to add is

67
00:02:42,000 --> 00:02:46,640
that also advanced topics like, unique

68
00:02:44,560 --> 00:02:48,160
pointers, shadow pointers,

69
00:02:46,640 --> 00:02:50,480
very complex

70
00:02:48,160 --> 00:02:52,720
templates will also not be part of the

71
00:02:50,480 --> 00:02:53,760
course because we have some limitations

72
00:02:52,720 --> 00:02:54,560
with time.

73
00:02:53,760 --> 00:02:58,000


74
00:02:54,560 --> 00:03:00,159
But of course in the end I will give and

75
00:02:58,000 --> 00:03:02,720
some links and stuff that you can

76
00:03:00,159 --> 00:03:05,280
continue and practice reverse

77
00:03:02,720 --> 00:03:06,400
engineering of C++ binaries. I

78
00:03:05,280 --> 00:03:07,200
will add

79
00:03:06,400 --> 00:03:10,080
that

80
00:03:07,200 --> 00:03:11,599
during this class, we are using a G++

81
00:03:10,080 --> 00:03:13,760
compiler.

82
00:03:11,599 --> 00:03:15,840
And in other compilers some of the

83
00:03:13,760 --> 00:03:18,879
things look a little bit different

84
00:03:15,840 --> 00:03:21,760
but because you will learn everything

85
00:03:18,879 --> 00:03:24,640
and about the concepts in this class and

86
00:03:21,760 --> 00:03:27,519
I will also try to show you a lot about

87
00:03:24,640 --> 00:03:30,239
how to use IDA, and how to define things.

88
00:03:27,519 --> 00:03:33,040
So, a lot of the things will be similar

89
00:03:30,239 --> 00:03:35,200
but some things will be different and it

90
00:03:33,040 --> 00:03:36,959
could be adjusted of course after you

91
00:03:35,200 --> 00:03:38,879
understand the concept you will see that

92
00:03:36,959 --> 00:03:39,920
the differences are not

93
00:03:38,879 --> 00:03:41,920
so

94
00:03:39,920 --> 00:03:44,799
big and you could adjust what you

95
00:03:41,920 --> 00:03:47,200
learned in other compilers too.

96
00:03:44,799 --> 00:03:50,560
And the last thing

97
00:03:47,200 --> 00:03:52,959
about like the general logistics and is

98
00:03:50,560 --> 00:03:56,879
that there are

99
00:03:52,959 --> 00:03:59,439
both hands-on exercises and videos

100
00:03:56,879 --> 00:04:01,760
and I would suggest to

101
00:03:59,439 --> 00:04:03,920
do all the exercises if you want to

102
00:04:01,760 --> 00:04:06,000
learn from the class and to have the

103
00:04:03,920 --> 00:04:09,280
best understanding of reversing

104
00:04:06,000 --> 00:04:10,879
in C++. So practice is main

105
00:04:09,280 --> 00:04:12,480
part of this

106
00:04:10,879 --> 00:04:15,519
journey that you take. So my

107
00:04:12,480 --> 00:04:16,479
recommendation is to follow and

108
00:04:15,519 --> 00:04:19,359
the

109
00:04:16,479 --> 00:04:22,160
class outline and both the videos and

110
00:04:19,359 --> 00:04:23,680
the hands-on exercises.

111
00:04:22,160 --> 00:04:25,919
Okay so,

112
00:04:23,680 --> 00:04:28,320
let's continue.

113
00:04:25,919 --> 00:04:30,400
So, the last thing that I would like to

114
00:04:28,320 --> 00:04:33,040
discuss is reverse engineering of

115
00:04:30,400 --> 00:04:34,880
C++ in general. Like why

116
00:04:33,040 --> 00:04:36,880
would you even want to take this class,

117
00:04:34,880 --> 00:04:38,720
like why is it different? So,

118
00:04:36,880 --> 00:04:40,880
there are some differences between

119
00:04:38,720 --> 00:04:42,479
reverse engineering C++ binaries

120
00:04:40,880 --> 00:04:43,280
and C binaries.

121
00:04:42,479 --> 00:04:45,440
And

122
00:04:43,280 --> 00:04:48,240
C++ as you know is the

123
00:04:45,440 --> 00:04:50,560
object-oriented language and there are a

124
00:04:48,240 --> 00:04:52,800
lot of complex data structures that are

125
00:04:50,560 --> 00:04:54,560
used in C++, for example

126
00:04:52,800 --> 00:04:57,040
inheritance which is something that we

127
00:04:54,560 --> 00:05:01,039
will discuss a lot during this class is

128
00:04:57,040 --> 00:05:03,039
a very big topic that is unique to 

129
00:05:01,039 --> 00:05:05,440
how you would like to reverse the

130
00:05:03,039 --> 00:05:06,639
C++ binaries that you have and to

131
00:05:05,440 --> 00:05:09,360
understand

132
00:05:06,639 --> 00:05:11,840
how to do a lot of things to

133
00:05:09,360 --> 00:05:14,880
understand how to do it in the best way

134
00:05:11,840 --> 00:05:17,840
and the most easy way to do it. So,

135
00:05:14,880 --> 00:05:19,759
this is why it's also important to

136
00:05:17,840 --> 00:05:22,160
understand the differences.

137
00:05:19,759 --> 00:05:24,960
And another thing is that

138
00:05:22,160 --> 00:05:25,919
in C++ there are also a lot of

139
00:05:24,960 --> 00:05:27,360
other

140
00:05:25,919 --> 00:05:29,680
complex and

141
00:05:27,360 --> 00:05:32,000
structures. If you talk about templates

142
00:05:29,680 --> 00:05:32,800
which is also something that is unique

143
00:05:32,000 --> 00:05:34,720
to

144
00:05:32,800 --> 00:05:36,960
what you would see in binaries with

145
00:05:34,720 --> 00:05:38,800
C++. 

146
00:05:36,960 --> 00:05:41,440
And also we have sharepoint and unique

147
00:05:38,800 --> 00:05:42,960
pointers and other things and that makes

148
00:05:41,440 --> 00:05:45,120
C++ 

149
00:05:42,960 --> 00:05:46,960
difficult to reverse.

150
00:05:45,120 --> 00:05:49,440
Of course

151
00:05:46,960 --> 00:05:52,320
we will also cover virtual calls during

152
00:05:49,440 --> 00:05:55,520
this class this is a very big topic

153
00:05:52,320 --> 00:05:58,000
because if you like look at

154
00:05:55,520 --> 00:06:00,240
the flow of the code, so

155
00:05:58,000 --> 00:06:01,280
you have many cases when

156
00:06:00,240 --> 00:06:04,960
the

157
00:06:01,280 --> 00:06:07,199
calls that you have are not direct calls

158
00:06:04,960 --> 00:06:09,680
but it will be indirect calls and this

159
00:06:07,199 --> 00:06:11,840
is a very big topic that makes C++ 

160
00:06:09,680 --> 00:06:13,759
harder to reverse,

161
00:06:11,840 --> 00:06:17,520
all of those things

162
00:06:13,759 --> 00:06:19,360
will be a part of the training and you

163
00:06:17,520 --> 00:06:21,120
would be able to

164
00:06:19,360 --> 00:06:22,479
learn and understand how to deal with

165
00:06:21,120 --> 00:06:24,479
those things.

166
00:06:22,479 --> 00:06:27,120
Another thing that is different in

167
00:06:24,479 --> 00:06:28,560
C++ is that there are

168
00:06:27,120 --> 00:06:29,600
new standards

169
00:06:28,560 --> 00:06:32,479
every

170
00:06:29,600 --> 00:06:36,240
some years and there are changes to the

171
00:06:32,479 --> 00:06:37,840
language every few years so also

172
00:06:36,240 --> 00:06:40,560
because the language is different the

173
00:06:37,840 --> 00:06:43,360
binaries will look different and

174
00:06:40,560 --> 00:06:45,919
you will have to adjust and learn all

175
00:06:43,360 --> 00:06:47,919
the time to

176
00:06:45,919 --> 00:06:48,880
understand the progress of the language

177
00:06:47,919 --> 00:06:51,440
and

178
00:06:48,880 --> 00:06:54,479
changes it makes on the binary.

179
00:06:51,440 --> 00:06:56,880
And this class will cover the basics

180
00:06:54,479 --> 00:06:58,160
that will be shared with all the current

181
00:06:56,880 --> 00:07:00,080
standards.

182
00:06:58,160 --> 00:07:02,880


183
00:07:00,080 --> 00:07:04,080
Another thing is that if you are in the

184
00:07:02,880 --> 00:07:06,400
field of

185
00:07:04,080 --> 00:07:08,800
vulnerability research, so

186
00:07:06,400 --> 00:07:11,599
vulnerabilities in C++ binaries

187
00:07:08,800 --> 00:07:13,120
are sometimes harder to find because

188
00:07:11,599 --> 00:07:16,080
there are a lot of things in the

189
00:07:13,120 --> 00:07:17,599
language that prevent the developer to

190
00:07:16,080 --> 00:07:20,319
make the

191
00:07:17,599 --> 00:07:22,639
mistakes that you would assume to see in

192
00:07:20,319 --> 00:07:25,199
C code. And of course there are

193
00:07:22,639 --> 00:07:28,080
vulnerabilities in

194
00:07:25,199 --> 00:07:30,400
everything, but in the end there are

195
00:07:28,080 --> 00:07:32,160
small differences when you do and

196
00:07:30,400 --> 00:07:34,560
vulnerability research for C++

197
00:07:32,160 --> 00:07:35,599
binaries and C binaries.

198
00:07:34,560 --> 00:07:37,680
And

199
00:07:35,599 --> 00:07:38,479
the last thing but the most important

200
00:07:37,680 --> 00:07:41,280
thing

201
00:07:38,479 --> 00:07:44,080
is that C++ reversing

202
00:07:41,280 --> 00:07:46,319
requires a lot of mapping and this is

203
00:07:44,080 --> 00:07:49,599
something that you would be

204
00:07:46,319 --> 00:07:51,120
seeing during this class in order to

205
00:07:49,599 --> 00:07:54,879
reverse engineer

206
00:07:51,120 --> 00:07:55,680
C++ and binary, you

207
00:07:54,879 --> 00:07:57,919
would

208
00:07:55,680 --> 00:08:00,000
have to define a lot of things and to

209
00:07:57,919 --> 00:08:02,560
understand a lot of the objects and the

210
00:08:00,000 --> 00:08:05,680
relationships in order to

211
00:08:02,560 --> 00:08:08,240
be better in reversing those binaries. So

212
00:08:05,680 --> 00:08:10,960
just keep it in mind and you would of

213
00:08:08,240 --> 00:08:13,680
course see it a lot during this class.

214
00:08:10,960 --> 00:08:14,879
But this is something that I think

215
00:08:13,680 --> 00:08:16,160


216
00:08:14,879 --> 00:08:18,479
you would

217
00:08:16,160 --> 00:08:20,319
need to keep in mind in order to

218
00:08:18,479 --> 00:08:21,680
do a better job in reversing C++

219
00:08:20,319 --> 00:08:22,960
plus binaries.

220
00:08:21,680 --> 00:08:25,599
So,

221
00:08:22,960 --> 00:08:28,160
I would like to thank you very much for

222
00:08:25,599 --> 00:08:30,800
taking the time and doing my course.

223
00:08:28,160 --> 00:08:32,080
I hope that you will both enjoy it and

224
00:08:30,800 --> 00:08:36,159
learn from me.

225
00:08:32,080 --> 00:08:36,159
And thank you very much

 

