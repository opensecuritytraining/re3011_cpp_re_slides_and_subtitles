1
00:00:01,610 --> 00:00:04,940
Our topic for today would
be object creation.

2
00:00:04,940 --> 00:00:08,646
And we will discuss both constructors

3
00:00:08,671 --> 00:00:13,619
and basic constructors and
also copy constructors.

4
00:00:13,620 --> 00:00:16,164
And we will also discuss
object creation,

5
00:00:16,175 --> 00:00:21,360
both objects created on the
stack and on the heap.

6
00:00:21,360 --> 00:00:26,440
So first, we would need to understand how
an object looks like in memory.

7
00:00:26,440 --> 00:00:30,940
So, in order to do that, we first need to
distinguish between two types of objects.

8
00:00:30,940 --> 00:00:35,110
And one of them is an object that
inherits or defines virtual functions,

9
00:00:35,135 --> 00:00:40,239
and the other one is an
object that doesn't.

10
00:00:40,239 --> 00:00:42,396
So, in case we have the second case,

11
00:00:42,421 --> 00:00:47,505
where an object doesn't have any
virtual functions in his creation,

12
00:00:47,530 --> 00:00:55,840
we would see the members of the object
being stored in the object's structure,

13
00:00:55,865 --> 00:00:59,182
one after another inside the constructor,

14
00:00:59,207 --> 00:01:01,630
and would not see any vtable.

15
00:01:01,631 --> 00:01:06,361
But in the other case, when we could
see that we do have a vtable,

16
00:01:06,386 --> 00:01:11,390
it would be in the first
four or eight bytes of the object.

17
00:01:11,390 --> 00:01:13,730
And it would depend
on the architecture.

18
00:01:13,730 --> 00:01:16,473
But in this case,
we would see both a vtable

19
00:01:16,485 --> 00:01:19,000
and afterwards we would see in memory all

20
00:01:19,000 --> 00:01:23,480
the other members of the object
stored one after another.

21
00:01:23,480 --> 00:01:30,090
There are other cases where the object would
have more complex structure in memory,

22
00:01:30,090 --> 00:01:35,140
but those cases would be discussed
in other parts of this training.

23
00:01:35,140 --> 00:01:41,540
And for now, we'll focus on the
basic and most common cases.

24
00:01:41,540 --> 00:01:45,046
So, another thing we
need to understand is

25
00:01:45,058 --> 00:01:49,709
because the constructor of the
object is very important,

26
00:01:49,734 --> 00:01:56,740
this is the function that's responsible for
setting up the object structure in memory.

27
00:01:56,740 --> 00:02:01,740
And this is the first function that would
be called when we want to create an object.

28
00:02:01,740 --> 00:02:04,773
So, it is important to recognize
that if we look at a function

29
00:02:04,798 --> 00:02:10,350
that what we are looking at is
actually the constructor of this object.

30
00:02:10,350 --> 00:02:12,531
So, how can we recognise it?

31
00:02:12,531 --> 00:02:16,515
So, there are like three basic things
that can kind of give you a hint

32
00:02:16,540 --> 00:02:22,141
at what you're looking at is
actually a constructor.

33
00:02:22,141 --> 00:02:28,910
So first, we would see that, in case we have
inherited or defined virtual functions,

34
00:02:28,935 --> 00:02:34,270
we would have the vtable stored in the first
4/8 bytes of the object's structure memory.

35
00:02:34,271 --> 00:02:37,837
So, this is something we would
see inside the constructor.

36
00:02:37,848 --> 00:02:42,940
We would see an assignment
and storing of the Vtable

37
00:02:42,965 --> 00:02:46,550
to the place in memory for the object.

38
00:02:46,551 --> 00:02:52,271
Later, we will see it can be 
on the stack or on the heap.

39
00:02:52,271 --> 00:02:56,301
Afterwards, we would see
also in the constructor,

40
00:02:56,326 --> 00:03:01,379
storing of many members inside
the object structure in memory.

41
00:03:01,379 --> 00:03:08,489
So, we would see storing of many types,
and sometimes, it will be like values

42
00:03:08,514 --> 00:03:11,881
that will be passed as
parameters for the constructors,

43
00:03:11,893 --> 00:03:16,671
but in some other cases,
it will be some kind of predefined values

44
00:03:16,696 --> 00:03:19,310
or like globals or stuff like that.

45
00:03:19,310 --> 00:03:24,209
But this is a behaviour that we would
expect to see in a constructor.

46
00:03:24,209 --> 00:03:27,716
Another thing that is also
involved with inheritance

47
00:03:27,741 --> 00:03:32,969
is that we can also see a call
to the base class constructor.

48
00:03:32,969 --> 00:03:38,709
So, this topic will be discussed
thoroughly in the inheritance topic.

49
00:03:38,709 --> 00:03:40,741
But I just like to give you another hint

50
00:03:40,766 --> 00:03:43,220
of what you would expect
to see in a constructor.

51
00:03:43,220 --> 00:03:46,409
So, you could also see
another function call

52
00:03:46,434 --> 00:03:49,896
inside the constructor
to another constructor.

53
00:03:49,921 --> 00:03:53,909
And this is also something that
really gives you a very good hint

54
00:03:53,935 --> 00:03:57,470
that what you're looking
at is actually a constructor.

55
00:03:57,471 --> 00:04:00,143
Of course, after you will reverse
engineer the whole function,

56
00:04:00,168 --> 00:04:02,065
you would understand it much better,

57
00:04:02,090 --> 00:04:04,680
and different constructors
could look differently.

58
00:04:04,681 --> 00:04:12,721
But this is the three key parts that many
constructors have inside of them.

59
00:04:12,721 --> 00:04:18,240
So, let's see what we understood
so far with an example.

60
00:04:18,240 --> 00:04:24,850
So, here you can see a snippet of some
assembly code of a constructor function.

61
00:04:24,850 --> 00:04:29,070
Object is actually a person
object that has two members.

62
00:04:29,070 --> 00:04:32,301
One of them is a name,
which is a char* - pointer.

63
00:04:32,301 --> 00:04:36,340
And the other one is an age, 
which is an integer.

64
00:04:36,340 --> 00:04:39,644
This object doesn't
have any virtual functions,

65
00:04:39,655 --> 00:04:44,199
because we want to focus on more
basic objects and constructors.

66
00:04:44,199 --> 00:04:50,550
So, let's see and examine
those assembly lines.

67
00:04:50,550 --> 00:04:55,965
So first, we can see that the parameters
are being passed in our rsi and edx,

68
00:04:55,990 --> 00:05:00,028
and they are in 8
bytes and 4 bytes registers,

69
00:05:00,053 --> 00:05:05,300
And this is actually the name and age.

70
00:05:05,300 --> 00:05:07,678
And before we see those parameters,

71
00:05:07,703 --> 00:05:13,150
the first actual parameter passed
to this function is the "This" pointer.

72
00:05:13,150 --> 00:05:19,879
"This" pointer is actually something
very common when we see C++ functions.

73
00:05:19,879 --> 00:05:24,589
Because, each time we have a function
that correlates with an object or class,

74
00:05:24,614 --> 00:05:27,456
what happens is that in
the first parameter,

75
00:05:27,481 --> 00:05:33,279
we have a pointer to the
object structure in memory.

76
00:05:33,280 --> 00:05:36,261
So, it can be a pointer on
the stack or on the heap,

77
00:05:36,286 --> 00:05:39,900
but it will be the
pointer of the object.

78
00:05:39,901 --> 00:05:43,910
So, you can see it both in the
constructor and the destructor,

79
00:05:43,910 --> 00:05:49,401
And also in any other function that
is the function of the object.

80
00:05:49,401 --> 00:05:53,775
So, this is something that
can actually give you a clue

81
00:05:53,800 --> 00:05:57,290
of what function correlates to which object.

82
00:05:57,290 --> 00:06:02,083
So. this is a very good tip
when you have some new binary

83
00:06:02,108 --> 00:06:06,551
that you need to map that has
many functions and many objects.

84
00:06:06,551 --> 00:06:12,000
So, I think this is something
that might be very helpful.

85
00:06:12,000 --> 00:06:14,716
So, after we see and
examine the parameters,

86
00:06:14,727 --> 00:06:18,899
we can focus on what actually
happened inside the constructor.

87
00:06:18,899 --> 00:06:21,901
So, this is a very
small and a basic one.

88
00:06:21,926 --> 00:06:28,092
But what we can see is that all of the
parameters that are listed as members,

89
00:06:28,117 --> 00:06:32,919
which means the name and
the age will be stored

90
00:06:32,944 --> 00:06:37,020
in the "This" pointer that was provided.

91
00:06:37,021 --> 00:06:42,040
So, you can see at first that
the name is being stored

92
00:06:42,065 --> 00:06:44,960
in offset 0 of the structure memory.

93
00:06:44,961 --> 00:06:52,929
And and you can see that afterwards,
we can see 4  bytes being stored of the age.

94
00:06:52,929 --> 00:06:57,145
So, we have in the memory,
eight bytes of the name,

95
00:06:57,170 --> 00:06:59,740
and then, four bytes of the age.

96
00:06:59,740 --> 00:07:03,198
And if you want to examine
the exact structure,

97
00:07:03,210 --> 00:07:07,379
so, this is actually the
structure of this object.

98
00:07:07,379 --> 00:07:12,158
So, we have a person object
with both a name and an age

99
00:07:12,183 --> 00:07:15,889
name is eight bytes, 
age is four bytes.

100
00:07:15,889 --> 00:07:18,979
So, we understood how a basic
constructor look like.

101
00:07:18,979 --> 00:07:24,539
But another thing we need to understand is
how our copy constructor look like.

102
00:07:24,539 --> 00:07:27,832
So, another function
you might encounter,

103
00:07:27,857 --> 00:07:33,639
when you do a reverse engineering of
C++ binaries is a copy constructor.

104
00:07:33,639 --> 00:07:40,499
A copy constructor actually creates an object
by copying the previously created one.

105
00:07:40,499 --> 00:07:45,824
So in many cases, you would see
something like this source code line

106
00:07:45,849 --> 00:07:49,410
when you have some
object being created,

107
00:07:49,435 --> 00:07:53,839
but it's being copied from
another created object.

108
00:07:53,840 --> 00:08:03,560
So, this function would probably be called
after you see another basic constructor call.

109
00:08:03,560 --> 00:08:12,158
And also, as a developer, in case you don't
explicitly write a copy constructor function,

110
00:08:12,183 --> 00:08:15,979
the compiler will
generate one for you.

111
00:08:15,980 --> 00:08:19,755
Because this is one of the functions
that the compiler will generate,

112
00:08:19,780 --> 00:08:22,020
no matter if you wrote it
explicitly or not.

113
00:08:22,020 --> 00:08:24,352
So the important part
is to understand

114
00:08:24,377 --> 00:08:27,520
how the copy constructor
looks like in the assembly.

115
00:08:27,521 --> 00:08:32,771
So, both the copy constructor and a basic
constructor we discussed previously.

116
00:08:32,771 --> 00:08:34,370
They will both allocate an object.

117
00:08:34,395 --> 00:08:40,862
They will both create an object 
in a specific place in memory.

118
00:08:40,887 --> 00:08:44,850
But there will be a main
difference regarding the parameter

119
00:08:44,875 --> 00:08:47,360
that's being passed for both of them.

120
00:08:47,360 --> 00:08:53,709
So, in this assembly example, you
can see some code with symbols.

121
00:08:53,709 --> 00:08:58,879
In other cases, we will not have symbols,
but the behaviour will be similar.

122
00:08:58,879 --> 00:09:05,040
So the parameters that are being passed to
the basic constructor are all the parameters

123
00:09:05,040 --> 00:09:09,589
that the constructor needs in
order to create an object.

124
00:09:09,589 --> 00:09:13,560
So besides the this point we
would also see all the other members.

125
00:09:13,585 --> 00:09:17,069
but in the other case,
the copy constructor,

126
00:09:17,094 --> 00:09:24,280
we can see that beside this, we would see
only another object pointer being passed.

127
00:09:24,280 --> 00:09:30,360
So, this is a way to distinguish that what we
are looking at is probably a copy constructor

128
00:09:30,360 --> 00:09:33,890
without even going inside the
function itself and analyze it.

129
00:09:33,890 --> 00:09:36,255
But of course, in some cases, 
it's not enough

130
00:09:36,267 --> 00:09:40,650
and we would need to actually go inside
the function, see what happens inside

131
00:09:40,675 --> 00:09:44,486
and see that it copies from a 
previously created object.

132
00:09:44,511 --> 00:09:50,138
but this is just like a small way that can
make your reversing process much easier

133
00:09:50,163 --> 00:09:53,575
to just look at the prameter try
to analyse it first,

134
00:09:53,600 --> 00:10:01,610
before diving into the function and
just like reverse everything inside.

135
00:10:01,610 --> 00:10:04,379
We talked previously about constructors,

136
00:10:04,391 --> 00:10:09,844
but another thing that is very important
to understand is object creation,

137
00:10:09,869 --> 00:10:13,189
an object could be created
both on the stack,

138
00:10:13,189 --> 00:10:17,750
and also, you can create
objects on the heap.

139
00:10:17,750 --> 00:10:22,307
And after the memory is
assigned for a specific object,

140
00:10:22,332 --> 00:10:27,410
a place to store the structure,
so, the constructor should be called.

141
00:10:27,411 --> 00:10:32,541
So, let's see a little bit more about
how you can create object on the heap.

142
00:10:32,541 --> 00:10:37,849
So, this is very simple line that
you probably saw in many cases,

143
00:10:37,874 --> 00:10:44,730
just like a new creation
of an object using 'new.'

144
00:10:44,730 --> 00:10:49,317
So, in this case, what happens behind the
scenes and assembly lines in the assembly code

145
00:10:49,342 --> 00:10:54,975
is that first you have the size
of the object being stored,

146
00:10:55,000 --> 00:10:58,930
and it is then passed
to 'operator new' function.

147
00:10:58,930 --> 00:11:06,391
In this case, the person object would have
16 bytes allocated in the memory.

148
00:11:06,391 --> 00:11:11,370
In cases when we have a structure
that is smaller than 16,

149
00:11:11,395 --> 00:11:15,950
there will be an alignment
of the size to 16.

150
00:11:15,950 --> 00:11:21,940
All the other alignments depends
on the compiler and architecture.

151
00:11:21,940 --> 00:11:25,519
But in our case, it will be 16.

152
00:11:25,519 --> 00:11:32,880
So, the second thing is the 'Person'.
The 'Person' actually in the end becomes the constructor call.

153
00:11:32,880 --> 00:11:36,085
So, we can see that prior
to the constructor call.

154
00:11:36,110 --> 00:11:38,440
We can see the parameter being set.

155
00:11:38,440 --> 00:11:40,445
So first, we have the name.

156
00:11:40,470 --> 00:11:45,740
We can also have the age and the
"This" pointer that we saw previously.

157
00:11:45,741 --> 00:11:49,909
So, this is in general how
object creation looks like

158
00:11:49,934 --> 00:11:52,811
when it's being allocated on the heap.

159
00:11:52,812 --> 00:11:58,880
And other things that we can see in many cases
is objects that are being stored on the stack.

160
00:11:58,880 --> 00:12:02,547
So, in some cases, the developer
or the compiler decides

161
00:12:02,572 --> 00:12:05,694
that the object should
be stored on the stack

162
00:12:05,719 --> 00:12:09,094
it is something that you can
also see in many cases.

163
00:12:09,120 --> 00:12:14,930
It's very similar to how an object looks
like when you allocate it on the heap.

164
00:12:14,931 --> 00:12:18,906
But the only difference is that
instead of just calling 'operator new'

165
00:12:18,931 --> 00:12:21,585
and just allocate the new object,

166
00:12:21,610 --> 00:12:26,864
you would see a simple
assignment of one of the registers

167
00:12:26,889 --> 00:12:29,450
to some location on the stack.

168
00:12:29,450 --> 00:12:32,620
So, this is what we would see in here.

169
00:12:32,620 --> 00:12:37,125
So, in many cases, you
would have other cases

170
00:12:37,150 --> 00:12:41,750
when you would not use 'new' because
C++ has different standards now.

171
00:12:41,775 --> 00:12:45,766
And in many cases, you would see shared
pointers, unique pointers

172
00:12:45,791 --> 00:12:47,999
and some other smart structures.

173
00:12:47,999 --> 00:12:54,540
And that would make your
object creation a little bit different.

174
00:12:54,540 --> 00:12:59,909
The assembly will also have
a little different behavior

175
00:12:59,934 --> 00:13:02,690
when you can see those things,

176
00:13:02,690 --> 00:13:06,155
but for now, we will discuss the
basic types of creating an object

177
00:13:06,180 --> 00:13:10,004
using 'new,' and then, allocated
on the heap manually,

178
00:13:10,029 --> 00:13:12,602
or an object created on the stack.

