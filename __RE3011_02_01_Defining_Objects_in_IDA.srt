1
00:00:00,719 --> 00:00:06,879
In this topic we will define a basic

2
00:00:03,600 --> 00:00:09,760
object in IDA. We discuss both how a

3
00:00:06,879 --> 00:00:12,880
different object structures look like

4
00:00:09,760 --> 00:00:15,360
but we also would see how to define the

5
00:00:12,880 --> 00:00:17,039
structures themselves and we would have

6
00:00:15,360 --> 00:00:18,560
a short demo

7
00:00:17,039 --> 00:00:24,000
to demonstrate how to create the

8
00:00:18,560 --> 00:00:24,000
structures and to show some examples.

9
00:00:24,640 --> 00:00:28,880
So first,

10
00:00:26,160 --> 00:00:30,080
we would need to do a short overview of

11
00:00:28,880 --> 00:00:32,559
what we learned

12
00:00:30,080 --> 00:00:35,040
previously. So, in the previous part of

13
00:00:32,559 --> 00:00:38,800
the training we talked about the object

14
00:00:35,040 --> 00:00:41,360
structure in memory. So we talked about

15
00:00:38,800 --> 00:00:44,559
different types of objects one of them

16
00:00:41,360 --> 00:00:47,360
is an object that has virtual functions

17
00:00:44,559 --> 00:00:48,160
inherited or defined, this object would

18
00:00:47,360 --> 00:00:50,879
have

19
00:00:48,160 --> 00:00:52,960
both vtable in the first four or

20
00:00:50,879 --> 00:00:54,000
eight bytes of the object structure in

21
00:00:52,960 --> 00:00:55,600
memory.

22
00:00:54,000 --> 00:00:59,039
And afterwards we will see all the

23
00:00:55,600 --> 00:01:00,719
members of the object in the other

24
00:00:59,039 --> 00:01:03,440
object we talked about we don't have

25
00:01:00,719 --> 00:01:06,080
virtual tables and in this case we would

26
00:01:03,440 --> 00:01:07,760
only see the members of the object in

27
00:01:06,080 --> 00:01:08,880
memory.

28
00:01:07,760 --> 00:01:09,760
So,

29
00:01:08,880 --> 00:01:10,640
in

30
00:01:09,760 --> 00:01:12,320

31
00:01:10,640 --> 00:01:14,720
future parts of the training you would

32
00:01:12,320 --> 00:01:17,040
also see more complex objects than what

33
00:01:14,720 --> 00:01:20,320
we described previously. But those would

34
00:01:17,040 --> 00:01:22,479
be discussed in the inheritance part

35
00:01:20,320 --> 00:01:24,400
also in some other parts of the training.

36
00:01:22,479 --> 00:01:27,280
But so far we will only discuss the

37
00:01:24,400 --> 00:01:31,040
basic objects, how to define them and how

38
00:01:27,280 --> 00:01:31,040
to understand, how they look like.

39
00:01:31,840 --> 00:01:37,280
So, if we want to define an object

40
00:01:34,400 --> 00:01:39,040
structure in IDA, we can do it using the

41
00:01:37,280 --> 00:01:42,640
structure window

42
00:01:39,040 --> 00:01:44,640
and that I will shortly show in the demo

43
00:01:42,640 --> 00:01:47,360
so we can press insert to create and

44
00:01:44,640 --> 00:01:50,799
define a new structure, we can rename the

45
00:01:47,360 --> 00:01:53,680
structure using N and we can add data

46
00:01:50,799 --> 00:01:55,920
structure members using D or A depends

47
00:01:53,680 --> 00:01:58,240
if you want an ASCII or not.

48
00:01:55,920 --> 00:02:00,399
You can also use Y in order to

49
00:01:58,240 --> 00:02:02,399
change the member types

50
00:02:00,399 --> 00:02:03,920
to whatever you want it can be

51
00:02:02,399 --> 00:02:07,439
another

52
00:02:03,920 --> 00:02:10,560
structure it can be some basic types. So

53
00:02:07,439 --> 00:02:14,400
this is something that you can also do.

54
00:02:10,560 --> 00:02:16,959
And now we would see a short demo to

55
00:02:14,400 --> 00:02:20,480
show you how to interact with IDA and to

56
00:02:16,959 --> 00:02:20,480
create the structure that you need.

57
00:02:21,520 --> 00:02:27,760
Okay, so this constructor

58
00:02:24,720 --> 00:02:29,840
in our demo is probably quite familiar

59
00:02:27,760 --> 00:02:31,760
from the previous part

60
00:02:29,840 --> 00:02:35,120
and so in the object creation part we

61
00:02:31,760 --> 00:02:37,280
discussed about this constructor and we

62
00:02:35,120 --> 00:02:39,920
now will show how to create the

63
00:02:37,280 --> 00:02:42,720
structure for it of course that when you

64
00:02:39,920 --> 00:02:45,840
create a structure for any other object

65
00:02:42,720 --> 00:02:47,200
it will look similar to what we do now.

66
00:02:45,840 --> 00:02:49,760
But

67
00:02:47,200 --> 00:02:51,440
just so we'll have a sense of how to do

68
00:02:49,760 --> 00:02:53,920
it yourself,

69
00:02:51,440 --> 00:02:54,720
first you can go to the structure tab in

70
00:02:53,920 --> 00:02:57,200
here

71
00:02:54,720 --> 00:03:01,280
if you don't see this tab specifically,

72
00:02:57,200 --> 00:03:03,599
you can go to view, open

73
00:03:01,280 --> 00:03:05,920
sub views, and then choose

74
00:03:03,599 --> 00:03:08,720
the right one. So in our case it would be

75
00:03:05,920 --> 00:03:11,680
the structure and

76
00:03:08,720 --> 00:03:12,959
here the structure ones, you can also use

77
00:03:11,680 --> 00:03:15,280
the

78
00:03:12,959 --> 00:03:16,560
short key which is Shift+F9

79
00:03:15,280 --> 00:03:19,120

80
00:03:16,560 --> 00:03:21,120
and then you would get this tab.

81
00:03:19,120 --> 00:03:22,319
So, when you come here you have here a

82
00:03:21,120 --> 00:03:25,280
short

83
00:03:22,319 --> 00:03:27,120
description of what you can do

84
00:03:25,280 --> 00:03:29,680
when you want to create a structure. I

85
00:03:27,120 --> 00:03:32,080
already talked about and do things in

86
00:03:29,680 --> 00:03:35,280
the presentation but we will see

87
00:03:32,080 --> 00:03:38,239
everything in a real example.

88
00:03:35,280 --> 00:03:40,080
So, when I press insert

89
00:03:38,239 --> 00:03:42,400
I get this

90
00:03:40,080 --> 00:03:44,879
window being opened

91
00:03:42,400 --> 00:03:47,680
and you can here choose the structure

92
00:03:44,879 --> 00:03:49,680
that you want. In our case it would be

93
00:03:47,680 --> 00:03:51,040
person

94
00:03:49,680 --> 00:03:53,519
because this is the name of the

95
00:03:51,040 --> 00:03:56,560
structure, there are some checkbox that

96
00:03:53,519 --> 00:03:58,480
we can choose. So, if you want to create a

97
00:03:56,560 --> 00:04:00,959
union instead of a structure this is

98
00:03:58,480 --> 00:04:04,080
also something that we can do.

99
00:04:00,959 --> 00:04:06,159
There are also other options so

100
00:04:04,080 --> 00:04:07,840
create before current structure so this

101
00:04:06,159 --> 00:04:09,680
is something you can choose where you

102
00:04:07,840 --> 00:04:11,680
want to put the structure

103
00:04:09,680 --> 00:04:13,519
because you can see in the background

104
00:04:11,680 --> 00:04:16,079
that there are other structures so you

105
00:04:13,519 --> 00:04:18,079
can choose this one, don't include in the

106
00:04:16,079 --> 00:04:20,959
list so this is another option that you

107
00:04:18,079 --> 00:04:23,120
have. But because we just want to create

108
00:04:20,959 --> 00:04:25,199
a standard structure

109
00:04:23,120 --> 00:04:27,680
so we can just do that.

110
00:04:25,199 --> 00:04:30,960
We can press ok and we get this empty

111
00:04:27,680 --> 00:04:32,479
structure with the name that we chose.

112
00:04:30,960 --> 00:04:34,400
We can also rename the structure

113
00:04:32,479 --> 00:04:36,720
afterwards using N,

114
00:04:34,400 --> 00:04:39,600
and then we have a pop-up with this

115
00:04:36,720 --> 00:04:40,840
window we can call it a different name,

116
00:04:39,600 --> 00:04:42,720
for example

117
00:04:40,840 --> 00:04:45,759

118
00:04:42,720 --> 00:04:46,639
we can call it Person_A.

119
00:04:45,759 --> 00:04:48,720

120
00:04:46,639 --> 00:04:50,400
And then you will have a different name,

121
00:04:48,720 --> 00:04:53,360
of course this is something that we

122
00:04:50,400 --> 00:04:56,000
don't want but it's a possibility.

123
00:04:53,360 --> 00:04:57,919
So the first thing that we 

124
00:04:56,000 --> 00:05:00,479
wanted to do is to create one but the

125
00:04:57,919 --> 00:05:03,600
second thing we want to do is to add

126
00:05:00,479 --> 00:05:05,039
members, in order to do that you can just

127
00:05:03,600 --> 00:05:07,360
press D.

128
00:05:05,039 --> 00:05:09,440
And a new field will appear here,

129
00:05:07,360 --> 00:05:11,759
you can see that the size of

130
00:05:09,440 --> 00:05:15,039
this field is one byte, you can see it

131
00:05:11,759 --> 00:05:17,199
here in the sizeof, here in the offset.

132
00:05:15,039 --> 00:05:19,919
But also you can see here that the type

133
00:05:17,199 --> 00:05:21,680
that IDA have for it, is one byte.

134
00:05:19,919 --> 00:05:24,080
If you want to make it larger you can

135
00:05:21,680 --> 00:05:25,360
just put your cursor on it, and press

136
00:05:24,080 --> 00:05:28,160
another

137
00:05:25,360 --> 00:05:30,639
a few times with the same key D, and it

138
00:05:28,160 --> 00:05:32,960
will change the size.

139
00:05:30,639 --> 00:05:36,080
You can also press Y.

140
00:05:32,960 --> 00:05:40,800
And define it so if we want to make it

141
00:05:36,080 --> 00:05:40,800
smaller you we can do like

142
00:05:40,960 --> 00:05:45,280
int and then you will have four bytes,

143
00:05:43,199 --> 00:05:49,199
like we defined in here.

144
00:05:45,280 --> 00:05:50,720
If for example we created us one byte,

145
00:05:49,199 --> 00:05:53,120
and then press Y.

146
00:05:50,720 --> 00:05:55,840
and it does an int and it automatically

147
00:05:53,120 --> 00:05:58,240
changes the size to 4 which is the

148
00:05:55,840 --> 00:06:02,400
right size for the integer.

149
00:05:58,240 --> 00:06:04,400
And in our person object like we can see,

150
00:06:02,400 --> 00:06:07,680
we can go back to the constructor and

151
00:06:04,400 --> 00:06:09,199
see here we have the name

152
00:06:07,680 --> 00:06:11,600
which is 8

153
00:06:09,199 --> 00:06:13,919
bytes, because it's a pointer and

154
00:06:11,600 --> 00:06:17,039
we have the age which is 4 bytes

155
00:06:13,919 --> 00:06:18,840
and because it's an integer. And this

156
00:06:17,039 --> 00:06:21,919
is what we want to create in our

157
00:06:18,840 --> 00:06:23,919
structure. And the structure is stored in

158
00:06:21,919 --> 00:06:24,960
rax, in this case

159
00:06:23,919 --> 00:06:26,880
and

160
00:06:24,960 --> 00:06:31,600
we can see that it's first storing the

161
00:06:26,880 --> 00:06:34,240
name and then it stores the age here.

162
00:06:31,600 --> 00:06:36,720
So, first we want to create

163
00:06:34,240 --> 00:06:39,039
an eight bytes of the name

164
00:06:36,720 --> 00:06:42,479
so we can change the size and then

165
00:06:39,039 --> 00:06:44,319
rename the field to name.

166
00:06:42,479 --> 00:06:45,360
Now, we have a name in the first eight

167
00:06:44,319 --> 00:06:48,880
bytes.

168
00:06:45,360 --> 00:06:52,080
Now, we can put our cursor at the end of

169
00:06:48,880 --> 00:06:54,560
the structure press D again and we will

170
00:06:52,080 --> 00:06:57,599
have a new field. So, when we have a new

171
00:06:54,560 --> 00:07:00,319
field we can change the size again

172
00:06:57,599 --> 00:07:03,120
so in this case it would be an integer

173
00:07:00,319 --> 00:07:04,880
so four bytes and we can rename it to

174
00:07:03,120 --> 00:07:07,360
age.

175
00:07:04,880 --> 00:07:10,639
After we rename both of them we can also

176
00:07:07,360 --> 00:07:12,400
change the type if I press Y,

177
00:07:10,639 --> 00:07:14,639
I get this

178
00:07:12,400 --> 00:07:17,680
window and I can change it to char

179
00:07:14,639 --> 00:07:20,720
pointer for example and

180
00:07:17,680 --> 00:07:23,360
I will have a small comment here in the

181
00:07:20,720 --> 00:07:24,800
end to show us that this is an offset to

182
00:07:23,360 --> 00:07:27,120
something else.

183
00:07:24,800 --> 00:07:29,360
So, it will help us to understand

184
00:07:27,120 --> 00:07:32,080
when we create structures. If we talk

185
00:07:29,360 --> 00:07:33,840
about like eight bytes

186
00:07:32,080 --> 00:07:38,560
for just like a

187
00:07:33,840 --> 00:07:42,400
member which is eight byte or a pointer.

188
00:07:38,560 --> 00:07:45,039
Here we have the age which is an integer,

189
00:07:42,400 --> 00:07:50,000
here we are talking about like

190
00:07:45,039 --> 00:07:51,520
x64 architectures, so the pointers

191
00:07:50,000 --> 00:07:54,479
are eight bytes.

192
00:07:51,520 --> 00:07:56,639
But, in case you would have x32

193
00:07:54,479 --> 00:07:59,360
 

194
00:07:56,639 --> 00:08:02,240
byte architecture. So, you would have a

195
00:07:59,360 --> 00:08:05,280
pointers that are four bytes. So, this is

196
00:08:02,240 --> 00:08:08,080
why my pointers here are eight bytes.

197
00:08:05,280 --> 00:08:10,560
So, now we define the small structure for

198
00:08:08,080 --> 00:08:13,440
the person object that we talked about

199
00:08:10,560 --> 00:08:15,360
and we can see here on the left that we

200
00:08:13,440 --> 00:08:18,240
also see the offsets

201
00:08:15,360 --> 00:08:19,440
of the different members inside of

202
00:08:18,240 --> 00:08:21,280

203
00:08:19,440 --> 00:08:23,759
our structure.

204
00:08:21,280 --> 00:08:25,199
If you want to see our structure

205
00:08:23,759 --> 00:08:27,440
in other

206
00:08:25,199 --> 00:08:28,800
tabs, so we can also see that in the

207
00:08:27,440 --> 00:08:31,280
local types

208
00:08:28,800 --> 00:08:33,599
we can see here

209
00:08:31,280 --> 00:08:35,279
the local types

210
00:08:33,599 --> 00:08:37,360
that if I do edit,

211
00:08:35,279 --> 00:08:40,479
I can see the

212
00:08:37,360 --> 00:08:44,080
definition of the object in a different

213
00:08:40,479 --> 00:08:46,080
way. You can also create objects this way

214
00:08:44,080 --> 00:08:48,240
using the local types and just like

215
00:08:46,080 --> 00:08:50,399
write what you need. This is very useful

216
00:08:48,240 --> 00:08:53,680
when you have some header file or

217
00:08:50,399 --> 00:08:56,000
something else that you want to take

218
00:08:53,680 --> 00:08:58,000
the information from or if you want to

219
00:08:56,000 --> 00:09:00,880
just write it much much quicker and

220
00:08:58,000 --> 00:09:04,080
without all the GUI options so this is

221
00:09:00,880 --> 00:09:05,680
also an option. You can import and export

222
00:09:04,080 --> 00:09:09,279
all of those structures you can see in

223
00:09:05,680 --> 00:09:12,320
the local types to your local structures,

224
00:09:09,279 --> 00:09:13,920
just by right clicking and then just

225
00:09:12,320 --> 00:09:16,480
like

226
00:09:13,920 --> 00:09:18,560
you can export to header file which is

227
00:09:16,480 --> 00:09:20,240
also an option but you can also

228
00:09:18,560 --> 00:09:23,360
like double click and then it will be

229
00:09:20,240 --> 00:09:26,080
added to your structures. So,

230
00:09:23,360 --> 00:09:28,000
there are a lot of options of how to

231
00:09:26,080 --> 00:09:29,279
deal with the local types and the

232
00:09:28,000 --> 00:09:30,160
structures.

233
00:09:29,279 --> 00:09:31,200
So,

234
00:09:30,160 --> 00:09:33,680

235
00:09:31,200 --> 00:09:36,640
this is in general how to create

236
00:09:33,680 --> 00:09:39,600
structures and deal with structures and

237
00:09:36,640 --> 00:09:42,160
you can see how easy it is to create

238
00:09:39,600 --> 00:09:45,120
the simple structure that we created and

239
00:09:42,160 --> 00:09:47,040
of course in other cases when you have

240
00:09:45,120 --> 00:09:48,320
other types of objects you can of course

241
00:09:47,040 --> 00:09:50,399
create

242
00:09:48,320 --> 00:09:52,000
using the structure window or the

243
00:09:50,399 --> 00:09:54,480
local types.

244
00:09:52,000 --> 00:09:57,519
And there are also options to create

245
00:09:54,480 --> 00:10:00,080
structures using IDApython or IDC,

246
00:09:57,519 --> 00:10:02,959
but this will not be covered

247
00:10:00,080 --> 00:10:06,240
in our part this time.

248
00:10:02,959 --> 00:10:09,040
So, as we showed in the demo and we

249
00:10:06,240 --> 00:10:10,800
created a simple structure

250
00:10:09,040 --> 00:10:12,720
using IDA

251
00:10:10,800 --> 00:10:13,920
in the structure

252
00:10:12,720 --> 00:10:17,200
window.

253
00:10:13,920 --> 00:10:20,480
And we had an object with two members

254
00:10:17,200 --> 00:10:22,000
and name and age and you can see that

255
00:10:20,480 --> 00:10:22,959
what we created looks something like

256
00:10:22,000 --> 00:10:25,600
this,

257
00:10:22,959 --> 00:10:28,399
and but after we create the structure we

258
00:10:25,600 --> 00:10:31,519
also need to correlate the assembly

259
00:10:28,399 --> 00:10:32,800
to the structure we created. So, in order

260
00:10:31,519 --> 00:10:36,480
to do that

261
00:10:32,800 --> 00:10:38,480
we have this option to right click on a

262
00:10:36,480 --> 00:10:41,600
specific place

263
00:10:38,480 --> 00:10:45,040
in the assembly. For example I have here

264
00:10:41,600 --> 00:10:47,680
a short snippet of assembly you can just

265
00:10:45,040 --> 00:10:48,800
right click on the offset from the

266
00:10:47,680 --> 00:10:52,000
register,

267
00:10:48,800 --> 00:10:54,800
and then choose the structure offset and

268
00:10:52,000 --> 00:10:57,120
choose the correct one in our case it

269
00:10:54,800 --> 00:10:58,959
will be the name and the age

270
00:10:57,120 --> 00:11:01,279
of the person object.

271
00:10:58,959 --> 00:11:03,519
And the assembly will look much much

272
00:11:01,279 --> 00:11:05,600
clearer afterwards.

273
00:11:03,519 --> 00:11:07,839
So, let's see

274
00:11:05,600 --> 00:11:09,440
another short demonstration using the

275
00:11:07,839 --> 00:11:12,399
demo

276
00:11:09,440 --> 00:11:14,880
to show you how to do it and how easy it

277
00:11:12,399 --> 00:11:17,920
is actually.

278
00:11:14,880 --> 00:11:20,720
So, after we created the structure as we

279
00:11:17,920 --> 00:11:23,040
said we want to correlate the assembly with

280
00:11:20,720 --> 00:11:26,079
the structure we created. I will show

281
00:11:23,040 --> 00:11:28,000
you how easy it can be,

282
00:11:26,079 --> 00:11:30,000
but I wanted to show you this in an

283
00:11:28,000 --> 00:11:31,920
interactive way so you can see what I

284
00:11:30,000 --> 00:11:35,040
need to do exactly in order to create

285
00:11:31,920 --> 00:11:38,399
the structure in the assembly itself.

286
00:11:35,040 --> 00:11:42,160
So, here in this line we can see that RDX,

287
00:11:38,399 --> 00:11:44,800
that has the name pointer will be stored

288
00:11:42,160 --> 00:11:46,880
in the first offset of the object

289
00:11:44,800 --> 00:11:49,120
structure in memory.

290
00:11:46,880 --> 00:11:51,200
So, in this case we know that this

291
00:11:49,120 --> 00:11:52,399
actually points to

292
00:11:51,200 --> 00:11:56,240
the name

293
00:11:52,399 --> 00:11:57,839
of the person. So, if I do right click

294
00:11:56,240 --> 00:11:59,040

295
00:11:57,839 --> 00:12:01,760

296
00:11:59,040 --> 00:12:04,320
I can see that I

297
00:12:01,760 --> 00:12:07,680
have the structure offset option

298
00:12:04,320 --> 00:12:09,279
and that I can choose the rax+Person.name, 

299
00:12:07,680 --> 00:12:11,360

300
00:12:09,279 --> 00:12:13,120
When I do that, that assembly becomes much

301
00:12:11,360 --> 00:12:15,519
much clearer and I don't need comments

302
00:12:13,120 --> 00:12:18,079
for everything and i can just see that

303
00:12:15,519 --> 00:12:21,839
what I'm looking at is actually the name.

304
00:12:18,079 --> 00:12:23,920
Of course we can also use the T,

305
00:12:21,839 --> 00:12:26,320
in the keyboard in order to do that but

306
00:12:23,920 --> 00:12:29,839
in order to make it more clear to you, I

307
00:12:26,320 --> 00:12:33,040
will do it with the mouse and with the GUI.

308
00:12:29,839 --> 00:12:35,200
So in this case in this line, we can also

309
00:12:33,040 --> 00:12:38,959
see an assignment in this case it would

310
00:12:35,200 --> 00:12:41,279
be the age that is being stored in edx

311
00:12:38,959 --> 00:12:43,920
you can see it in here and in here.

312
00:12:41,279 --> 00:12:45,680
And I want to change this assembly line,

313
00:12:43,920 --> 00:12:46,880
so what I need to do is just like right

314
00:12:45,680 --> 00:12:49,120
click

315
00:12:46,880 --> 00:12:51,760
go to offset structure

316
00:12:49,120 --> 00:12:54,880
and then choose the person.age.

317
00:12:51,760 --> 00:12:57,440
So, after we define our person

318
00:12:54,880 --> 00:12:59,360
structure in here

319
00:12:57,440 --> 00:13:02,480
you can also do it in the IDA view

320
00:12:59,360 --> 00:13:04,480
and just like edit the assembly lines

321
00:13:02,480 --> 00:13:05,839
and the offsets to be

322
00:13:04,480 --> 00:13:07,760

323
00:13:05,839 --> 00:13:09,839
what relevant and

324
00:13:07,760 --> 00:13:12,000
help you a lot when you do the

325
00:13:09,839 --> 00:13:15,279
reversing process.

326
00:13:12,000 --> 00:13:17,279
So, that's it regarding defining basic

327
00:13:15,279 --> 00:13:18,639
objects in IDA.

328
00:13:17,279 --> 00:13:21,839

329
00:13:18,639 --> 00:13:24,720
I hope it was clear, and that and now you

330
00:13:21,839 --> 00:13:28,399
can do an exercise. If you felt like some

331
00:13:24,720 --> 00:13:30,320
of the things were a little bit

332
00:13:28,399 --> 00:13:32,480
new to you maybe you should come back

333
00:13:30,320 --> 00:13:37,279
and afterwards continue with the

334
00:13:32,480 --> 00:13:37,279
exercise. So good luck!!
 

