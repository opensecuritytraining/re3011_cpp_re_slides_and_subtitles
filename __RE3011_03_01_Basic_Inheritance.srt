1
00:00:00,640 --> 00:00:08,080
Our topic will be basic inheritance.
We discussed previously about basic

2
00:00:08,080 --> 00:00:13,759
constructors and how to identify and
recognize them in the assembly code.

3
00:00:13,759 --> 00:00:18,640
We
examined a few options to how you can

4
00:00:18,640 --> 00:00:24,240
recognize that what you are actually
looking at is a constructor.

5
00:00:24,240 --> 00:00:28,840
And
in this part, we will focus mostly on the

6
00:00:28,840 --> 00:00:33,600
changes of
what happens when a constructor is a

7
00:00:33,600 --> 00:00:37,680
constructor
of a derived class or a base class

8
00:00:37,680 --> 00:00:42,399
and what happens when we have virtual
functions involved.

9
00:00:42,399 --> 00:00:48,719
So first, we would have to discuss the
virtual tables. I briefly talked

10
00:00:48,719 --> 00:00:53,920
about what is exactly the vtable but
we will dive into this more thoroughly

11
00:00:53,920 --> 00:00:55,840
this time.
So,

12
00:00:55,840 --> 00:01:01,520
the vtable is used to support dynamic
dispatch and in general the action of

13
00:01:01,520 --> 00:01:05,920
finding the right function
that should be executed when we are

14
00:01:05,920 --> 00:01:10,640
doing a virtual call.
So for every class that inherits or

15
00:01:10,640 --> 00:01:15,680
defined virtual functions,
we have a vtable set to the first 8

16
00:01:15,680 --> 00:01:20,080
bytes or 4 bytes of the object
structure in memory.

17
00:01:20,080 --> 00:01:24,000
And
this is done using the relevant

18
00:01:24,000 --> 00:01:30,079
constructor of the object
the virtual table actually stores a

19
00:01:30,079 --> 00:01:34,079
pointer to the relevant definition of
each virtual function and

20
00:01:34,079 --> 00:01:37,840
all the other functions that are used by
this object.

21
00:01:37,840 --> 00:01:42,159
So, when a virtual call occurs what
happens is that

22
00:01:42,159 --> 00:01:45,840
the correct function is being chosen
from that.

23
00:01:45,840 --> 00:01:51,200
Okay so
after we understood the basics and we

24
00:01:51,200 --> 00:01:54,399
already discussed about them in the
previous parts,

25
00:01:54,399 --> 00:01:58,159
we would also want to talk about
inheritance.

26
00:01:58,159 --> 00:02:04,479
Inheritance is a big part of C++
binaries, it is used in almost every

27
00:02:04,479 --> 00:02:09,440
C++ binary that I researched.
And I think this is very important part

28
00:02:09,440 --> 00:02:13,840
because
when you do a reversing of C++,

29
00:02:13,840 --> 00:02:19,440
mapping of the inheritance is very
important because we have many

30
00:02:19,440 --> 00:02:24,160
objects and the binary has a lot of
relationship between them and the

31
00:02:24,160 --> 00:02:29,120
virtual calls and everything depends on
this. So, understanding how to map the

32
00:02:29,120 --> 00:02:33,519
relationship tree of the object that we
have in the binary is very important and

33
00:02:33,519 --> 00:02:37,519
will save us a lot of time during the
research afterwards.

34
00:02:37,519 --> 00:02:41,760
So,
for that we would see an example

35
00:02:41,760 --> 00:02:45,040
of
two objects that inherits from

36
00:02:45,040 --> 00:02:48,160
each other.
And

37
00:02:48,160 --> 00:02:53,120
we would examine an example with symbols
I think that because we are in the

38
00:02:53,120 --> 00:02:57,760
beginning of our understanding of
inheritance and C++ objects, it's

39
00:02:57,760 --> 00:03:01,200
important that we will have an example
that could really

40
00:03:01,200 --> 00:03:07,040
teach us how to understand those things
and how to do them by yourself

41
00:03:07,040 --> 00:03:11,519
afterwards in the exercises and in real
life binaries.

42
00:03:11,519 --> 00:03:17,599
So, in order to do that, we will do the
first explanation

43
00:03:17,599 --> 00:03:21,599
on
the "symboled" example

44
00:03:21,599 --> 00:03:25,680
afterwards you would also see a
walkthroughs and exercises that will not

45
00:03:25,680 --> 00:03:30,159
have symbols anymore.
But for this part, I think it will be

46
00:03:30,159 --> 00:03:33,280
much clearer and better to understand in
this way.

47
00:03:33,280 --> 00:03:39,760
So in our example we have a spaceship
that inherits form a game object.

48
00:03:39,760 --> 00:03:44,239
In order to understand
this relationship between the object, we

49
00:03:44,239 --> 00:03:48,640
will examine first the constructor of
the spaceship.

50
00:03:48,640 --> 00:03:53,200
So, what we can see and this is something
we already discussed a little bit in the

51
00:03:53,200 --> 00:03:59,360
previous parts is that we can see a call
to the base class constructor. So we can

52
00:03:59,360 --> 00:04:05,120
see inside the constructor some function
call that is unclear to us now.

53
00:04:05,120 --> 00:04:09,040
But we will examine it afterwards
more. So

54
00:04:09,040 --> 00:04:12,879
remember this game object function
call

55
00:04:12,879 --> 00:04:19,359
afterwards we have the vtable set for
the eight bytes of the object and this

56
00:04:19,359 --> 00:04:24,080
is stored in the first eight bytes in
the object structure in memory.

57
00:04:24,080 --> 00:04:29,360
After understanding the object structure
using the constructor, we can examine its

58
00:04:29,360 --> 00:04:33,600
vtable too.
So, we can see the vtable of spaceship in

59
00:04:33,600 --> 00:04:36,320
here
but before we dive in the functions

60
00:04:36,320 --> 00:04:40,320
themselves we can see in the beginning
that there is some RTTI Complete Object

61
00:04:40,320 --> 00:04:46,560
Locator and we probably want to know
what is this thing. So, RTTI is a

62
00:04:46,560 --> 00:04:53,120
runtime type information and it exposes
the object data types at runtime. And

63
00:04:53,120 --> 00:04:56,880
this is something that is very
common in C++ vtables

64
00:04:56,880 --> 00:05:00,880
and any operation on an object requires
us to

65
00:05:00,880 --> 00:05:05,919
have a pointer or reference
and to the suitable type for the objects.

66
00:05:05,919 --> 00:05:10,560
So, this is how
the RTTI is used in here. There are also

67
00:05:10,560 --> 00:05:15,120
other usages for RTTI for example
dynamic casting but dynamic casting will

68
00:05:15,120 --> 00:05:19,520
not be part of this class
and we will focus more on the relevant

69
00:05:19,520 --> 00:05:25,280
parts for our training.
So, after understanding what

70
00:05:25,280 --> 00:05:31,280
is actually RTTI, we can dive into the
functions that we see in this vtable.

71
00:05:31,280 --> 00:05:35,840
So, we can see here that the vtable
contains both functions that are game

72
00:05:35,840 --> 00:05:40,639
object functions and also one function
that is a spaceship

73
00:05:40,639 --> 00:05:45,039
object function. So, we have the spaceship
move objects and we have a lot of other

74
00:05:45,039 --> 00:05:49,600
game object functions.
So, because we have symbols we can see

75
00:05:49,600 --> 00:05:54,000
which function
correlates to which object

76
00:05:54,000 --> 00:05:57,759
in like
other examples you usually just have the

77
00:05:57,759 --> 00:06:03,520
function name like
auto-generated name just like IDA do.

78
00:06:03,520 --> 00:06:07,600
That's just like
sub and underscore and then just a

79
00:06:07,600 --> 00:06:13,120
number and in those cases you would not
know which function actually correlates

80
00:06:13,120 --> 00:06:18,080
to which object. In those cases you would
have to go to the functions and research

81
00:06:18,080 --> 00:06:22,080
the vtables and the functions
themselves to understand what function

82
00:06:22,080 --> 00:06:27,360
is relevant to which object. I would show
you during this part also how we

83
00:06:27,360 --> 00:06:31,919
examined the vtable of the base class
constructor and this would also show us

84
00:06:31,919 --> 00:06:38,880
how we can actually connect
what function is relevant to which one.

85
00:06:38,880 --> 00:06:41,039
So,

86
00:06:41,840 --> 00:06:46,080
in this case we can see both the
game object function and we would like

87
00:06:46,080 --> 00:06:50,560
to also see the constructor of the game
object to understand exactly those

88
00:06:50,560 --> 00:06:54,319
connections between the
functions of game object and the

89
00:06:54,319 --> 00:06:59,199
functions of spaceship. So, after we
examine the spaceship vtable, we will

90
00:06:59,199 --> 00:07:04,560
now examine the game object constructor
and the game object vtable in order to

91
00:07:04,560 --> 00:07:10,400
understand those relationships between
the object and also to see more how to

92
00:07:10,400 --> 00:07:14,240
understand the vtable that we showed
previously.

93
00:07:14,240 --> 00:07:19,120
So, the game object assigns the vtable
to the first eight bytes

94
00:07:19,120 --> 00:07:25,039
of its object structure in memory
and this is something that

95
00:07:25,039 --> 00:07:29,039
makes a lot of sense. Because in the
previous part we saw that spaceship

96
00:07:29,039 --> 00:07:34,160
that is the derived class of game object
does have a vtable and usually when we

97
00:07:34,160 --> 00:07:39,360
have a derived class with vtable, we
usually also have a base class with a

98
00:07:39,360 --> 00:07:43,599
vtable because many of the virtual
functions are

99
00:07:43,599 --> 00:07:46,960
like actually being defined in the base
class.

100
00:07:46,960 --> 00:07:51,120
So,
the game object constructor and members

101
00:07:51,120 --> 00:07:57,360
so those parts are actually something we
saw in many examples and exercises

102
00:07:57,360 --> 00:08:01,520
during this class.
And we can see here

103
00:08:01,520 --> 00:08:03,440
that
we have

104
00:08:03,440 --> 00:08:07,759
two members that are being initialized
in those function calls.

105
00:08:07,759 --> 00:08:12,479
And afterwards, we can see that
the members are being stored

106
00:08:12,479 --> 00:08:16,720
in the
offset 8 and offset 12 of the object

107
00:08:16,720 --> 00:08:21,520
structure memory.
In this case what happens is that

108
00:08:21,520 --> 00:08:27,120
because those are actually integers
the size of those members would be 4

109
00:08:27,120 --> 00:08:30,479
bytes.
After we examine the members we also

110
00:08:30,479 --> 00:08:35,279
have a vtable
of the game object and we can see that

111
00:08:35,279 --> 00:08:40,959
there is one a pure virtual call in this
virtual table.

112
00:08:40,959 --> 00:08:45,040
And what happens here is that actually
because we have symbols, what happens is

113
00:08:45,040 --> 00:08:50,080
that we have this definition that IDA
adds that this actually

114
00:08:50,080 --> 00:08:54,480
is a pure call.
But in cases that we don't have

115
00:08:54,480 --> 00:08:58,959
symbols in the binary we would
sometimes see the pure code, but in other

116
00:08:58,959 --> 00:09:04,240
cases we would see just a 0
in the assembly. So, this is something

117
00:09:04,240 --> 00:09:09,760
like a pattern of behavior, you would see.
And another thing is that

118
00:09:09,760 --> 00:09:13,920
if you remember from the previous
vtable that we examined, is that in this

119
00:09:13,920 --> 00:09:18,240
exact location what we
have is actually the spaceship function

120
00:09:18,240 --> 00:09:21,440
the move object function.
So this

121
00:09:21,440 --> 00:09:27,519
means that the game object defined
a virtual function, and the derived class

122
00:09:27,519 --> 00:09:34,399
in our case it is spaceship defined
the function and implemented it. So, this

123
00:09:34,399 --> 00:09:38,880
is actually why we can see this behavior.
All the other functions are actually the

124
00:09:38,880 --> 00:09:45,440
same as the vtable of the spaceship.
I will add that in cases when we don't

125
00:09:45,440 --> 00:09:48,880
have symbols,
one of the things that

126
00:09:48,880 --> 00:09:52,959
happens is that
you examine the vtables

127
00:09:52,959 --> 00:09:57,440
and you look at the virtual functions
and functions that you can see there and

128
00:09:57,440 --> 00:10:00,480
try to
like cross reference between them and

129
00:10:00,480 --> 00:10:05,120
see what functions are the same and what
functions are different. This is a very

130
00:10:05,120 --> 00:10:10,640
good way to identify what functions are
actually the same functions and the

131
00:10:10,640 --> 00:10:14,640
relevant function of the base class, and
what functions was implemented in the

132
00:10:14,640 --> 00:10:18,560
derived class.
So, this is something that usually

133
00:10:18,560 --> 00:10:22,160
needs to be done in order to map the
binaries.

134
00:10:22,160 --> 00:10:25,680
And this is something that is very
useful when you do that in order to

135
00:10:25,680 --> 00:10:29,440
understand like
what functions are relevant to which

136
00:10:29,440 --> 00:10:35,120
object and also to understand the
relationship a little bit better.

137
00:10:35,120 --> 00:10:38,800
So, if you want to summarize everything
that we learned so far.

138
00:10:38,800 --> 00:10:43,920
So, when we're first reversing a C++
binary it is very important to map

139
00:10:43,920 --> 00:10:49,440
the relationship between objects. This
will save you a lot of time during the

140
00:10:49,440 --> 00:10:53,600
ongoing research, because you would have
understanding of the big picture, you

141
00:10:53,600 --> 00:10:56,320
could use those structures that you
create

142
00:10:56,320 --> 00:10:59,839
during the
research of the functions and just like

143
00:10:59,839 --> 00:11:05,440
add them to the assembly so you would be
able to examine the code and binary much

144
00:11:05,440 --> 00:11:09,360
more easily.
One of the things that you could do in

145
00:11:09,360 --> 00:11:14,160
order to map the relationship is
actually looking at the constructor. We

146
00:11:14,160 --> 00:11:19,200
focused a lot on constructors
during the last parts

147
00:11:19,200 --> 00:11:21,680
and
in order to

148
00:11:21,680 --> 00:11:26,320
identify the inheritance which function
is

149
00:11:26,320 --> 00:11:30,800
the constructor of the derived class,
which one is of the base class, how to

150
00:11:30,800 --> 00:11:35,600
understand the inheritance tree. So this
is something that you can see from the

151
00:11:35,600 --> 00:11:39,839
constructors and the vtables.
And also

152
00:11:39,839 --> 00:11:44,240
the virtual tables that we talked about
will help you understand the virtual

153
00:11:44,240 --> 00:11:48,959
calls in the binary. This is something
that is very problematic and hard to

154
00:11:48,959 --> 00:11:53,200
understand when you look at the binary
of C++. Especially, when you don't

155
00:11:53,200 --> 00:11:58,720
execute the binary and look in runtime
what happens. And the virtual calls

156
00:11:58,720 --> 00:12:04,000
would be covered in the next part
and we will talk about how to identify

157
00:12:04,000 --> 00:12:08,639
them and how to understand them better.
So thank you very much,

158
00:12:08,639 --> 00:12:11,600
and good luck!!
 

