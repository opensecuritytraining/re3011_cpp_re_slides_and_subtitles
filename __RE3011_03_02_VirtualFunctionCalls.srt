1
00:00:00,000 --> 00:00:03,840
This part will cover virtual function

2
00:00:02,399 --> 00:00:06,960
calls

3
00:00:03,840 --> 00:00:09,920
and we talked previously about virtual

4
00:00:06,960 --> 00:00:12,160
calls and about vtables

5
00:00:09,920 --> 00:00:14,719
and specifically in the inheritance part

6
00:00:12,160 --> 00:00:18,400
and also in the object creation path.

7
00:00:14,719 --> 00:00:21,039
And in this section we will cover more

8
00:00:18,400 --> 00:00:23,680
the practical how to deal with virtual

9
00:00:21,039 --> 00:00:27,519
calls, how they look like in the assembly

10
00:00:23,680 --> 00:00:30,080
and also would cover with a demo a

11
00:00:27,519 --> 00:00:32,880
real example that we will see one of

12
00:00:30,080 --> 00:00:36,480
the virtual calls, we will statically go

13
00:00:32,880 --> 00:00:38,559
over the assembly and we'll try to find

14
00:00:36,480 --> 00:00:40,079
the correct function that was called in

15
00:00:38,559 --> 00:00:41,040
this scenario.

16
00:00:40,079 --> 00:00:44,960
So

17
00:00:41,040 --> 00:00:48,800
let's start with the example so

18
00:00:44,960 --> 00:00:53,039
in many cases when we see an assembly

19
00:00:48,800 --> 00:00:53,920
code and it actually has virtual calls

20
00:00:53,039 --> 00:00:56,160
and

21
00:00:53,920 --> 00:00:58,480
with a short and very simple example

22
00:00:56,160 --> 00:00:59,920
I'll i will show you how it looks like

23
00:00:58,480 --> 00:01:02,800
in the assembly

24
00:00:59,920 --> 00:01:06,320
with a short overview of the beginning.

25
00:01:02,800 --> 00:01:09,840
So in this code we have a very simple

26
00:01:06,320 --> 00:01:12,400
code that has a person being created

27
00:01:09,840 --> 00:01:15,200
and there is a virtual call

28
00:01:12,400 --> 00:01:18,560
called, 'printAge()' this

29
00:01:15,200 --> 00:01:21,920
virtual function was

30
00:01:18,560 --> 00:01:22,799
defined in the class creation.

31
00:01:21,920 --> 00:01:26,720
And

32
00:01:22,799 --> 00:01:29,360
if we can compile this code and in the

33
00:01:26,720 --> 00:01:31,680
function call of the 'printAge()', we would

34
00:01:29,360 --> 00:01:34,320
have actually a virtual call.

35
00:01:31,680 --> 00:01:35,040
So, if you want to examine the assembly

36
00:01:34,320 --> 00:01:36,479
so

37
00:01:35,040 --> 00:01:38,880
first

38
00:01:36,479 --> 00:01:42,720
we have the pointer to the person which

39
00:01:38,880 --> 00:01:45,520
is our object and we examined the

40
00:01:42,720 --> 00:01:46,880
structure of the object so it has the

41
00:01:45,520 --> 00:01:49,600
vtable

42
00:01:46,880 --> 00:01:51,920
in the first 8 bytes. Then we have

43
00:01:49,600 --> 00:01:53,280
all the members of the object stored

44
00:01:51,920 --> 00:01:56,079
afterwards.

45
00:01:53,280 --> 00:01:58,000
But this is in general how the

46
00:01:56,079 --> 00:02:00,719
pointer looks like

47
00:01:58,000 --> 00:02:03,520
and afterwards we have the assignment of

48
00:02:00,719 --> 00:02:05,680
the vtable to the first 8 bytes of

49
00:02:03,520 --> 00:02:08,239
the object structure in memory. So we can

50
00:02:05,680 --> 00:02:10,800
see here in this slide that the vtable

51
00:02:08,239 --> 00:02:13,840
is stored in RAX.

52
00:02:10,800 --> 00:02:15,280
Then we can see later that we have a

53
00:02:13,840 --> 00:02:17,520
virtual call.

54
00:02:15,280 --> 00:02:19,360
Now in this case we don't have any

55
00:02:17,520 --> 00:02:21,760
members, but

56
00:02:19,360 --> 00:02:24,720
we will focus more on the virtual call

57
00:02:21,760 --> 00:02:27,520
and how to identify and see what happens.

58
00:02:24,720 --> 00:02:32,000
So, in this case you can see that the

59
00:02:27,520 --> 00:02:34,400
virtual call is actually taking the

60
00:02:32,000 --> 00:02:37,040
vtable pointer and then choose a

61
00:02:34,400 --> 00:02:40,080
specific offset, in this case it will be

62
00:02:37,040 --> 00:02:40,879
8, and then it calls this function.

63
00:02:40,080 --> 00:02:43,040
So

64
00:02:40,879 --> 00:02:45,599
how we can really understand that. So you

65
00:02:43,040 --> 00:02:49,040
can see we are using brackets ("[]")

66
00:02:45,599 --> 00:02:52,080
on the RAX+8. So what happens is

67
00:02:49,040 --> 00:02:54,640
that we access the value in this address.

68
00:02:52,080 --> 00:02:55,920
And this address actually points to the

69
00:02:54,640 --> 00:02:58,640
vtable.

70
00:02:55,920 --> 00:03:01,280
So what happens here is that the

71
00:02:58,640 --> 00:03:02,560
function that was called in our case was

72
00:03:01,280 --> 00:03:05,920
print age,

73
00:03:02,560 --> 00:03:09,680
so it is being taken from the vtable

74
00:03:05,920 --> 00:03:12,239
and then it is being executed.

75
00:03:09,680 --> 00:03:14,720
Now, the offset in this case is 8, it

76
00:03:12,239 --> 00:03:16,879
can be like different offset depends on

77
00:03:14,720 --> 00:03:19,840
what function you need, but this is

78
00:03:16,879 --> 00:03:21,120
in general how a virtual function

79
00:03:19,840 --> 00:03:22,159
call look like.

80
00:03:21,120 --> 00:03:24,560

81
00:03:22,159 --> 00:03:27,920
We only have like four lines of assembly

82
00:03:24,560 --> 00:03:30,879
here, but in a lot of binaries this is a

83
00:03:27,920 --> 00:03:33,120
very very hard job to understand what

84
00:03:30,879 --> 00:03:34,959
exactly is the function that was called.

85
00:03:33,120 --> 00:03:36,640
So let's

86
00:03:34,959 --> 00:03:40,080
continue with the virtual calls and

87
00:03:36,640 --> 00:03:41,440
understand how to examine a more complex

88
00:03:40,080 --> 00:03:43,680
assembly

89
00:03:41,440 --> 00:03:47,920
code and binary

90
00:03:43,680 --> 00:03:50,239
with a short demo. So after we

91
00:03:47,920 --> 00:03:53,680
saw some examples

92
00:03:50,239 --> 00:03:56,239
and we examined the basic virtual call

93
00:03:53,680 --> 00:03:58,319
and we now will see what happens when we

94
00:03:56,239 --> 00:03:59,200
look at a real binary

95
00:03:58,319 --> 00:04:02,480
and

96
00:03:59,200 --> 00:04:04,080
how we can identify and understand the

97
00:04:02,480 --> 00:04:06,159
virtual call

98
00:04:04,080 --> 00:04:09,120
that we want to analyze.

99
00:04:06,159 --> 00:04:10,239
So here I took an example

100
00:04:09,120 --> 00:04:12,239
when

101
00:04:10,239 --> 00:04:13,200
we go over this code we can see that in

102
00:04:12,239 --> 00:04:15,519
here

103
00:04:13,200 --> 00:04:18,479
we have a virtual call

104
00:04:15,519 --> 00:04:22,240
and this virtual call

105
00:04:18,479 --> 00:04:24,720
vtable and function is stored in RAX.

106
00:04:22,240 --> 00:04:26,800
And in order to understand what

107
00:04:24,720 --> 00:04:30,080
exactly is the function that was called

108
00:04:26,800 --> 00:04:32,080
in this case, we would need to go further

109
00:04:30,080 --> 00:04:34,720
and analyze

110
00:04:32,080 --> 00:04:36,479
the binary and the assembly code in

111
00:04:34,720 --> 00:04:39,280
order to understand

112
00:04:36,479 --> 00:04:42,720
who assigns the

113
00:04:39,280 --> 00:04:45,199
vtable in RAX and what exactly is

114
00:04:42,720 --> 00:04:50,240
stored in offset 8.

115
00:04:45,199 --> 00:04:53,919
So, in this case, if I mark RAX. I can go

116
00:04:50,240 --> 00:04:58,639
back and see that RAX is the return value

117
00:04:53,919 --> 00:05:01,759
of this function call in here.

118
00:04:58,639 --> 00:05:03,919
So, because RAX is the return value of

119
00:05:01,759 --> 00:05:05,520
this function, we need to understand what

120
00:05:03,919 --> 00:05:06,720
exactly was

121
00:05:05,520 --> 00:05:09,360
returned

122
00:05:06,720 --> 00:05:11,280
and with this information we can

123
00:05:09,360 --> 00:05:14,479
understand and analyze

124
00:05:11,280 --> 00:05:16,000
the virtual call that we have.

125
00:05:14,479 --> 00:05:18,320
In this case

126
00:05:16,000 --> 00:05:20,479
the function call is

127
00:05:18,320 --> 00:05:22,240
using a shared pointer.

128
00:05:20,479 --> 00:05:24,160
Shared pointer

129
00:05:22,240 --> 00:05:26,160
is something that will not be covered

130
00:05:24,160 --> 00:05:29,680
during this class

131
00:05:26,160 --> 00:05:33,600
but a shared pointer

132
00:05:29,680 --> 00:05:36,720
is a feature that provides the option to

133
00:05:33,600 --> 00:05:39,440
have an object that can be shared

134
00:05:36,720 --> 00:05:40,800
and have a few

135
00:05:39,440 --> 00:05:41,840
instances

136
00:05:40,800 --> 00:05:44,320
and

137
00:05:41,840 --> 00:05:46,080
in this case, I will not dive

138
00:05:44,320 --> 00:05:48,320
further into

139
00:05:46,080 --> 00:05:51,039
how to analyze those shared pointers and

140
00:05:48,320 --> 00:05:53,039
what can be done with them. But I would

141
00:05:51,039 --> 00:05:56,960
add that this

142
00:05:53,039 --> 00:06:01,120
function call would return to us the

143
00:05:56,960 --> 00:06:02,080
spaceship object pointer.

144
00:06:01,120 --> 00:06:06,960
So,

145
00:06:02,080 --> 00:06:10,560
in RAX we would have the pointer to

146
00:06:06,960 --> 00:06:14,080
a spaceship object, so we can rename this

147
00:06:10,560 --> 00:06:15,680
local variable to a pointer to

148
00:06:14,080 --> 00:06:17,120
spaceship.

149
00:06:15,680 --> 00:06:19,360
After we

150
00:06:17,120 --> 00:06:22,800
see that this local variable stores the

151
00:06:19,360 --> 00:06:25,440
pointer that we need, and we can see that

152
00:06:22,800 --> 00:06:29,120
further in this line

153
00:06:25,440 --> 00:06:30,400
the RAX pointer would access the first

154
00:06:29,120 --> 00:06:33,360
offset

155
00:06:30,400 --> 00:06:36,639
and we'll store it in RAX this means that

156
00:06:33,360 --> 00:06:39,520
this line would take the vtable from

157
00:06:36,639 --> 00:06:42,800
the object that we have here and store

158
00:06:39,520 --> 00:06:45,600
it in RAX. Later on like we

159
00:06:42,800 --> 00:06:48,960
see in this line, there is another access

160
00:06:45,600 --> 00:06:51,599
to the vtable with offset 8

161
00:06:48,960 --> 00:06:53,919
and this would be

162
00:06:51,599 --> 00:06:57,039
the function that would be called.

163
00:06:53,919 --> 00:06:59,599
So, in order to analyze the vtable

164
00:06:57,039 --> 00:07:02,560
and the functions inside of it

165
00:06:59,599 --> 00:07:04,240
we would need to analyze the spaceship

166
00:07:02,560 --> 00:07:06,240
object first.

167
00:07:04,240 --> 00:07:09,680
So, you might remember the spaceship

168
00:07:06,240 --> 00:07:11,759
object from previous parts of this class

169
00:07:09,680 --> 00:07:14,080
but I would go over the constructor of

170
00:07:11,759 --> 00:07:17,680
spaceship in order to find the vtable

171
00:07:14,080 --> 00:07:19,039
and the correct function. So if I open

172
00:07:17,680 --> 00:07:19,840
the function

173
00:07:19,039 --> 00:07:25,680

174
00:07:19,840 --> 00:07:25,680
window, I can choose a spaceship

175
00:07:29,039 --> 00:07:32,720
and this is the spaceship constructor. So

176
00:07:31,840 --> 00:07:35,680
I

177
00:07:32,720 --> 00:07:35,680
pressed the

178
00:07:36,400 --> 00:07:41,599
ctrl+p in order to open this

179
00:07:38,639 --> 00:07:43,680
window. And now I can choose the

180
00:07:41,599 --> 00:07:45,759
constructor.

181
00:07:43,680 --> 00:07:47,440
So this is the wrapper. I will go to the

182
00:07:45,759 --> 00:07:49,680
constructor itself.

183
00:07:47,440 --> 00:07:50,879
Now, we have the constructor of the

184
00:07:49,680 --> 00:07:53,199
spaceship.

185
00:07:50,879 --> 00:07:55,840
But we are not interested in all the

186
00:07:53,199 --> 00:07:59,599
constructor parts, we are only interested

187
00:07:55,840 --> 00:08:01,680
in the vtable part of this constructor.

188
00:07:59,599 --> 00:08:05,120
So, we can see that in this

189
00:08:01,680 --> 00:08:07,280
line. We are assigning the vtable

190
00:08:05,120 --> 00:08:10,160
and here we can see that it is being

191
00:08:07,280 --> 00:08:13,680
stored in the first offset of the

192
00:08:10,160 --> 00:08:16,319
spaceship object. So, now we

193
00:08:13,680 --> 00:08:18,319
understand that this is the relevant

194
00:08:16,319 --> 00:08:20,400
vtable that we need and this is what we

195
00:08:18,319 --> 00:08:23,280
will analyze.

196
00:08:20,400 --> 00:08:27,440
So, if I go to the address of the

197
00:08:23,280 --> 00:08:28,960
vtable. I can see and that this is the

198
00:08:27,440 --> 00:08:32,399
vtable that

199
00:08:28,960 --> 00:08:33,680
is used for the virtual call

200
00:08:32,399 --> 00:08:34,959
that we have.

201
00:08:33,680 --> 00:08:37,919
So, the first

202
00:08:34,959 --> 00:08:41,039
function is this one game object role

203
00:08:37,919 --> 00:08:42,800
this is something that a you might

204
00:08:41,039 --> 00:08:46,480
be familiar with but

205
00:08:42,800 --> 00:08:49,200
exactly as I said previously, a spaceship

206
00:08:46,480 --> 00:08:51,519
inherits from a game object

207
00:08:49,200 --> 00:08:54,320
and this is something we covered in the

208
00:08:51,519 --> 00:08:57,120
basic inheritance part.

209
00:08:54,320 --> 00:08:58,320
And we can see that this one is a

210
00:08:57,120 --> 00:08:59,360

211
00:08:58,320 --> 00:09:02,000

212
00:08:59,360 --> 00:09:02,880
virtual function call and this will

213
00:09:02,000 --> 00:09:04,560
be

214
00:09:02,880 --> 00:09:07,200
in offset 8,

215
00:09:04,560 --> 00:09:10,640
Which means the second function.

216
00:09:07,200 --> 00:09:13,440
So, after we analyze the vtable

217
00:09:10,640 --> 00:09:15,920
and we analyze also the virtual call, we

218
00:09:13,440 --> 00:09:19,279
know that this function is the function

219
00:09:15,920 --> 00:09:21,680
that was called from the

220
00:09:19,279 --> 00:09:23,920
virtual call

221
00:09:21,680 --> 00:09:24,800
that we analyze from here.

222
00:09:23,920 --> 00:09:27,120
So

223
00:09:24,800 --> 00:09:29,519
if we want to

224
00:09:27,120 --> 00:09:30,720
make it clear we can add a comment.

225
00:09:29,519 --> 00:09:31,920
Okay

226
00:09:30,720 --> 00:09:34,480
this is

227
00:09:31,920 --> 00:09:37,120
the virtual call that was that can be

228
00:09:34,480 --> 00:09:39,360
called here and

229
00:09:37,120 --> 00:09:41,600
and we can say okay we're done. But

230
00:09:39,360 --> 00:09:43,920
because we don't want to leave the

231
00:09:41,600 --> 00:09:46,080
assembly code like this even though we

232
00:09:43,920 --> 00:09:48,959
understood what function would be used

233
00:09:46,080 --> 00:09:50,880
in this case and we do want to have a

234
00:09:48,959 --> 00:09:53,200
more

235
00:09:50,880 --> 00:09:54,399
like a clean, more

236
00:09:53,200 --> 00:09:56,800
built

237
00:09:54,399 --> 00:09:57,839
solution for those cases when we have

238
00:09:56,800 --> 00:10:00,959
vtables.

239
00:09:57,839 --> 00:10:02,079
In order to do that, what we can do is

240
00:10:00,959 --> 00:10:03,760
go back

241
00:10:02,079 --> 00:10:04,480
to

242
00:10:03,760 --> 00:10:06,720
the

243
00:10:04,480 --> 00:10:08,480
vtable.

244
00:10:06,720 --> 00:10:12,079
And we can create

245
00:10:08,480 --> 00:10:14,959
a structure of the vtable and then

246
00:10:12,079 --> 00:10:17,519
change the assembly offset to point to

247
00:10:14,959 --> 00:10:20,079
the correct function in the vtable. So

248
00:10:17,519 --> 00:10:22,959
if we create the structure

249
00:10:20,079 --> 00:10:26,320
for the vtable for each iteration and

250
00:10:22,959 --> 00:10:28,399
each function call that would use this

251
00:10:26,320 --> 00:10:32,720
vtable, we can just like change the

252
00:10:28,399 --> 00:10:33,519
assembly to point to the correct

253
00:10:32,720 --> 00:10:35,680

254
00:10:33,519 --> 00:10:36,880
element in our

255
00:10:35,680 --> 00:10:38,959
structure.

256
00:10:36,880 --> 00:10:41,360
And I will show you

257
00:10:38,959 --> 00:10:43,839
the easiest way to do so.

258
00:10:41,360 --> 00:10:46,160
So, of course you can open a structure by

259
00:10:43,839 --> 00:10:48,000
yourself and just like add each function

260
00:10:46,160 --> 00:10:48,880
that you can see in here in this

261
00:10:48,000 --> 00:10:51,360
vtable.

262
00:10:48,880 --> 00:10:52,640
But, I think it will take too much

263
00:10:51,360 --> 00:10:54,560
time.

264
00:10:52,640 --> 00:10:56,880
You have the option to do it with the

265
00:10:54,560 --> 00:10:59,519
script, so you can use either IDApython or

266
00:10:56,880 --> 00:11:02,079
IDC in order to do that or use some

267
00:10:59,519 --> 00:11:04,640
header file if you have it.

268
00:11:02,079 --> 00:11:07,040
But if you don't have it and vtable

269
00:11:04,640 --> 00:11:07,839
is too large to do it manually or

270
00:11:07,040 --> 00:11:10,959

271
00:11:07,839 --> 00:11:13,440
if you don't know how to write scripts yet,

272
00:11:10,959 --> 00:11:17,040
so you could just like mark the vtable

273
00:11:13,440 --> 00:11:19,839
that you want to create a structure for.

274
00:11:17,040 --> 00:11:22,720
Press right-click, and then you can

275
00:11:19,839 --> 00:11:24,959
create structure form selection.

276
00:11:22,720 --> 00:11:26,480
This will create the structure that you

277
00:11:24,959 --> 00:11:29,279
want

278
00:11:26,480 --> 00:11:31,839
in the structure tab and from vtable

279
00:11:29,279 --> 00:11:34,480
that we marked. Of course this option is

280
00:11:31,839 --> 00:11:36,640
valid for a lot of other things

281
00:11:34,480 --> 00:11:38,720
in the binary. So also you can create

282
00:11:36,640 --> 00:11:40,000
structure from other selections that are not

283
00:11:38,720 --> 00:11:43,600
vtables.

284
00:11:40,000 --> 00:11:45,200
So try it sometimes when you

285
00:11:43,600 --> 00:11:47,920
work on different binaries and you would

286
00:11:45,200 --> 00:11:49,120
see how helpful it could be.

287
00:11:47,920 --> 00:11:51,920
So,

288
00:11:49,120 --> 00:11:54,480
I pressed like the create structure and

289
00:11:51,920 --> 00:11:57,279
now we have a structure here with all

290
00:11:54,480 --> 00:11:59,279
the functions that

291
00:11:57,279 --> 00:12:02,000
we saw previously.

292
00:11:59,279 --> 00:12:04,800
I would add that this function look a

293
00:12:02,000 --> 00:12:05,600
little bit weird because this is using

294
00:12:04,800 --> 00:12:08,160
the

295
00:12:05,600 --> 00:12:10,639
mangled names

296
00:12:08,160 --> 00:12:13,279
and if you remember in the beginning

297
00:12:10,639 --> 00:12:15,279
of the training, I told you that if we

298
00:12:13,279 --> 00:12:17,279
want to see the names differently we can

299
00:12:15,279 --> 00:12:20,399
press the demangled names

300
00:12:17,279 --> 00:12:21,839
and then change it to names, it will make

301
00:12:20,399 --> 00:12:24,079

302
00:12:21,839 --> 00:12:26,959
binary much clearer and you can see the

303
00:12:24,079 --> 00:12:28,399
functions in a more human readable way.

304
00:12:26,959 --> 00:12:29,760

305
00:12:28,399 --> 00:12:31,440

306
00:12:29,760 --> 00:12:34,079
Because we do it automatically this is

307
00:12:31,440 --> 00:12:36,399
how it creates those things, even though

308
00:12:34,079 --> 00:12:38,399
it looks different you can see that the

309
00:12:36,399 --> 00:12:40,480
name of the functions are clear

310
00:12:38,399 --> 00:12:42,959
just like the variables and arguments

311
00:12:40,480 --> 00:12:43,839
are all part of this

312
00:12:42,959 --> 00:12:44,800
thing.

313
00:12:43,839 --> 00:12:46,639

314
00:12:44,800 --> 00:12:48,880
I will not cover how exactly it will be

315
00:12:46,639 --> 00:12:50,880
mangled or not, but I think that for an

316
00:12:48,880 --> 00:12:52,639
automatic solution it will be good

317
00:12:50,880 --> 00:12:55,360
enough to understand what we are looking

318
00:12:52,639 --> 00:12:58,480
at. Of course, again scripts can always

319
00:12:55,360 --> 00:12:59,920
help you get it more clear and

320
00:12:58,480 --> 00:13:02,079
much much

321
00:12:59,920 --> 00:13:03,519
easier to understand but for now we will

322
00:13:02,079 --> 00:13:05,600
go with this.

323
00:13:03,519 --> 00:13:08,000
So, this is just like a random structure

324
00:13:05,600 --> 00:13:11,360
with an address. We will change the name

325
00:13:08,000 --> 00:13:15,120
so we know what exactly is the structure.

326
00:13:11,360 --> 00:13:18,760
So in our case this will be a struct for

327
00:13:15,120 --> 00:13:18,760
a spaceship

328
00:13:18,880 --> 00:13:21,839
vtable,

329
00:13:21,920 --> 00:13:26,560

330
00:13:23,839 --> 00:13:29,200
and press, “ok”. So, now we have this

331
00:13:26,560 --> 00:13:32,399
structure with the virtual

332
00:13:29,200 --> 00:13:34,160
table. Let's go back, so I will show

333
00:13:32,399 --> 00:13:37,120
you how you can use this structure

334
00:13:34,160 --> 00:13:39,839
inside your virtual call

335
00:13:37,120 --> 00:13:42,800
in the assembly. 

336
00:13:39,839 --> 00:13:42,800
Just move to here,

337
00:13:45,519 --> 00:13:49,639
I want to go back to the–

338
00:13:51,760 --> 00:13:56,240
okay.

339
00:13:52,959 --> 00:13:58,560
So now we are back on the correct

340
00:13:56,240 --> 00:14:01,440
place in the assembly, so we have the

341
00:13:58,560 --> 00:14:04,000
comment here but because we created the

342
00:14:01,440 --> 00:14:07,360
structure we can now right click to a

343
00:14:04,000 --> 00:14:10,320
structure offset then choose

344
00:14:07,360 --> 00:14:13,440
this vtable that we created.

345
00:14:10,320 --> 00:14:16,240
Again because we already had the 

346
00:14:13,440 --> 00:14:17,920
vtable, we can see that here we have

347
00:14:16,240 --> 00:14:20,639
the name of the

348
00:14:17,920 --> 00:14:23,600
virtual call, that will be used in this

349
00:14:20,639 --> 00:14:26,320
case and now it is not that hard to

350
00:14:23,600 --> 00:14:29,839
analyze this part of the assembly.

351
00:14:26,320 --> 00:14:31,279
Now, I will add a few parts before

352
00:14:29,839 --> 00:14:32,399
we finished this

353
00:14:31,279 --> 00:14:35,120
demo.

354
00:14:32,399 --> 00:14:37,920
In this case we only work with

355
00:14:35,120 --> 00:14:40,320
static analysis after binary, if we would

356
00:14:37,920 --> 00:14:42,639
want to do dynamic analysis of course

357
00:14:40,320 --> 00:14:46,079
that the work would be much faster but

358
00:14:42,639 --> 00:14:48,959
in many cases, you would not be able to

359
00:14:46,079 --> 00:14:51,839
debug and execute your binary. If you do

360
00:14:48,959 --> 00:14:53,680
malware or if you do embedded firmware,

361
00:14:51,839 --> 00:14:54,399
so sometimes it's not something you can

362
00:14:53,680 --> 00:14:57,600
do

363
00:14:54,399 --> 00:14:59,920
that easily and for that we would also

364
00:14:57,600 --> 00:15:01,839
need to understand how to do everything

365
00:14:59,920 --> 00:15:05,360
statically.

366
00:15:01,839 --> 00:15:08,079
So, in this case and what we did

367
00:15:05,360 --> 00:15:10,959
and just to summarize everything we

368
00:15:08,079 --> 00:15:13,600
saw that we have a virtual function

369
00:15:10,959 --> 00:15:17,680
call in here, we went backward and

370
00:15:13,600 --> 00:15:20,320
understood from where the vtable was

371
00:15:17,680 --> 00:15:22,720
used and what set up vtable and

372
00:15:20,320 --> 00:15:25,440
then we just went to the constructor

373
00:15:22,720 --> 00:15:26,639
analyzed the vtable, found the correct

374
00:15:25,440 --> 00:15:29,360
function

375
00:15:26,639 --> 00:15:32,160
with the correct offset, it created the

376
00:15:29,360 --> 00:15:35,600
structure and now we correlated the

377
00:15:32,160 --> 00:15:37,279
assembly with the structure we created.

378
00:15:35,600 --> 00:15:40,320
So,

379
00:15:37,279 --> 00:15:41,519
I choose like a example that is not too

380
00:15:40,320 --> 00:15:44,160
large and

381
00:15:41,519 --> 00:15:47,279
that because we have symbols it's much

382
00:15:44,160 --> 00:15:49,199
easier. But, think about other cases when

383
00:15:47,279 --> 00:15:52,160
you have a lot of virtual calls without

384
00:15:49,199 --> 00:15:54,800
symbols this can be very complex. So

385
00:15:52,160 --> 00:15:58,399
virtual calls is a very important

386
00:15:54,800 --> 00:16:01,279
subject to understand when you do C++

387
00:15:58,399 --> 00:16:03,519
reversing. Because this would repeat

388
00:16:01,279 --> 00:16:06,240
itself in a lot of

389
00:16:03,519 --> 00:16:08,399
places in the binary and in most of the

390
00:16:06,240 --> 00:16:10,959
binaries like you would have virtual

391
00:16:08,399 --> 00:16:13,040
calls. So, it's important to understand

392
00:16:10,959 --> 00:16:16,000
that it's just like a matter of like

393
00:16:13,040 --> 00:16:19,759
understanding the whole hierarchy of the

394
00:16:16,000 --> 00:16:22,000
object to understand which object is

395
00:16:19,759 --> 00:16:24,160
where and where is the constructor and

396
00:16:22,000 --> 00:16:26,399
then from then, you can just like analyze

397
00:16:24,160 --> 00:16:27,759
the vtable and understand the virtual

398
00:16:26,399 --> 00:16:30,079
call.

399
00:16:27,759 --> 00:16:31,920
So, thank you very much I hope you

400
00:16:30,079 --> 00:16:34,800
learned from it and

401
00:16:31,920 --> 00:16:34,800
good luck!!
 

