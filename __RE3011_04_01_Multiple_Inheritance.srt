1
00:00:01,360 --> 00:00:08,640
Our topic today would be multiple inheritances. 
And regarding the agenda, we would first have an  

2
00:00:08,640 --> 00:00:16,080
introduction to the multiple inheritance concept 
and then we will discuss how constructors look  

3
00:00:16,080 --> 00:00:23,600
like in the binaries and how we can recognize 
the constructors, how we can understand the object  

4
00:00:24,720 --> 00:00:32,720
and how it looks like in the memory. And we will 
also examine an example to explain everything that  

5
00:00:33,280 --> 00:00:41,680
we learned during this class. So, as an introduction 
and we first need to understand what is exactly  

6
00:00:41,680 --> 00:00:49,440
multiple inheritances. So, I assume that many of 
you already encountered multiple inheritance  

7
00:00:49,440 --> 00:00:54,880
when writing code but as an overview, I will 
explain a little bit what happens in these cases.  

8
00:00:55,520 --> 00:01:01,920
So, in multiple inheritance is a case when you 
have a class that inherits from more than one  

9
00:01:01,920 --> 00:01:09,440
other classes. Which means like in the example that 
I showed in the slide, it says we have for  

10
00:01:09,440 --> 00:01:16,320
example class A and it can inherit from more than 
one base class. In this case, it will inherit from  

11
00:01:16,320 --> 00:01:26,320
both class B and class C. So, sometimes 
when analyzing binaries you can see some cases  

12
00:01:26,320 --> 00:01:35,360
of inheritance that you might mistaken 
as regular inheritance or basic inheritance.  

13
00:01:35,360 --> 00:01:42,000
But, eventually after analyzing the constructor 
you'll see that the patterns and behavior inside  

14
00:01:42,000 --> 00:01:49,600
a constructor is actually a more suitable for 
multiple inheritance case. So, because we would  

15
00:01:49,600 --> 00:01:56,560
see those things a lot in binaries during your 
time and when you research code you would  

16
00:01:56,560 --> 00:02:03,440
actually need to understand the fundamental 
parts of how to analyze multiple inheritances,  

17
00:02:03,440 --> 00:02:09,440
how to examine it and how to identify that what 
you are looking at is actually a derived class  

18
00:02:10,320 --> 00:02:18,160
that has more than one base class. So, now we will 
discuss the constructor. So, the constructor  

19
00:02:18,160 --> 00:02:25,120
of when you have multiple inheritances is quite 
similar to the constructor when you have a basic  

20
00:02:25,120 --> 00:02:31,680
inheritance. There are a few differences between 
the two, and we will discuss what you can see in  

21
00:02:31,680 --> 00:02:37,280
the constructor that can explain to you that what 
you are looking at is actually the derived class  

22
00:02:38,640 --> 00:02:44,960
in the case of multiple inheritances. So, what you 
would typically see is that you would  

23
00:02:44,960 --> 00:02:51,280
have more than one constructor call inside the 
constructor of the derived class. Which means you have  

24
00:02:51,840 --> 00:02:58,320
more than one call to the base class constructor. 
This actually makes sense because if you have  

25
00:02:58,320 --> 00:03:04,240
more than one base class, so you would have 
to call more than one constructor. Another  

26
00:03:04,240 --> 00:03:11,440
thing you would see is actually that the member 
assignments are quite similar. You would see that  

27
00:03:11,440 --> 00:03:17,840
you still have the same behavior when storing the 
members inside the object's structure and memory.  

28
00:03:18,880 --> 00:03:25,600
But, the order of the members would be a little bit 
different than what you would assume. I would show  

29
00:03:26,880 --> 00:03:32,560
shortly how it would look like and we will have 
a full explanation about why it's like that, and  

30
00:03:32,560 --> 00:03:39,840
how you could analyze the object. And another thing 
that it's important to understand when examining  

31
00:03:39,840 --> 00:03:46,880
constructors is that, when you have multiple 
inheritance you would in many cases have more than  

32
00:03:46,880 --> 00:03:53,600
one vtable. Which means that in case you have 
virtual functions that are defined or inherited,  

33
00:03:53,600 --> 00:04:00,640
so there would be more than one vtable in the 
object structure. So like I explained previously,  

34
00:04:00,640 --> 00:04:06,160
we talked about like what we would assume to see 
in the constructor and this actually explains  

35
00:04:06,160 --> 00:04:12,240
a lot about how the object would actually look 
like in memory. So, in this case, we take the simple  

36
00:04:12,240 --> 00:04:18,960
example of class A that inherits from class B and 
class C and we will examine how the derived object  

37
00:04:18,960 --> 00:04:27,680
looks like when we examine its object in memory. So, 
first we will look at class B, and when we look at  

38
00:04:27,680 --> 00:04:35,120
the derived object structure, we would first see 
call in the constructor to the constructor of  

39
00:04:35,120 --> 00:04:43,360
class B. So, you would first see a vtable that has 
both the functions that are relevant for the base  

40
00:04:43,360 --> 00:04:49,680
class and which is class B and also the functions 
that are relevant from the derived class A.  

41
00:04:50,720 --> 00:04:57,440
Afterwards, we would see all the class B 
members stored one after another inside  

42
00:04:57,440 --> 00:05:04,720
the object. So, this is actually marked here that 
you can see what parts are relevant for class B.  

43
00:05:05,920 --> 00:05:12,640
After class B, we would actually have to 
deal with the second part of the inheritance  

44
00:05:12,640 --> 00:05:21,360
which is the second inherited class, in this 
case it would be class C. So, the base class here  

45
00:05:21,360 --> 00:05:27,840
also have a vtable, and you would see 
in the object structure that you would have  

46
00:05:27,840 --> 00:05:34,160
first vtable that has the relevant 
function from the base class C. And  

47
00:05:34,160 --> 00:05:40,480
also the function relevant from the derived 
class A. After the vtable we would also see  

48
00:05:40,480 --> 00:05:45,840
a similar pattern that we will see all the 
other members that are relevant for class C

49
00:05:48,400 --> 00:05:55,680
and of course, we would also have to deal 
with the derived class specific changes  

50
00:05:55,680 --> 00:06:02,560
and members. So, besides the virtual functions
that would be implemented in A, that are part of  

51
00:06:02,560 --> 00:06:10,800
the vtables. We would also have the members that 
are created in class A, stored in their object  

52
00:06:11,760 --> 00:06:18,480
memory. So this is in general how a derived 
class would look like in memory after the  

53
00:06:18,480 --> 00:06:25,040
constructor call and this is what a construction 
tool would actually create. I would also mention  

54
00:06:25,040 --> 00:06:33,280
that if there are no virtual functions so of 
course the vtable would be unnecessary in those  

55
00:06:33,280 --> 00:06:39,760
cases, and will not be part of the constructor. But, 
when we talk about multiple inheritance so, this  

56
00:06:39,760 --> 00:06:45,920
is very unlikely to happen. But, just keep it in 
mind in case you would see something like that.  

57
00:06:45,920 --> 00:06:55,520
So, after we explained about how we would assume to 
analyze and see the multiple inheritance objects, 

58
00:06:55,520 --> 00:07:01,680
we would also examine a specific example with 
some assembly code and we would be able to  

59
00:07:01,680 --> 00:07:07,520
overview everything that we learned so far. So, in 
our case, we will have a CoffeeTable that inherits  

60
00:07:07,520 --> 00:07:15,360
from both a Furniture class and a Table class. 
So, in here we can see that there is an assembly  

61
00:07:15,360 --> 00:07:21,120
snippet and that is part of the constructor of 
the CoffeeTable class that we discussed about.  

62
00:07:21,840 --> 00:07:28,080
So, we would see some patterns that you might be 
familiar with from the previous parts of the basic  

63
00:07:28,080 --> 00:07:34,720
inheritance classes, but we would also examine the 
differences and what happens to the "this" pointer  

64
00:07:34,720 --> 00:07:41,120
during the constructor 
execution. So, one of the most likely  

65
00:07:42,400 --> 00:07:50,320
unique thing that you can see here is that, 
there are two constructor calls in here. So we  

66
00:07:50,320 --> 00:07:57,680
both have the first call to Furniture constructor, 
and then we would have the Table constructor. 

67
00:07:57,680 --> 00:08:04,720
As you understand both of them are base classes 
for the derived class in our case CoffeeTable. 

68
00:08:06,000 --> 00:08:13,520
So, after we have those two constructor calls 
we would see that the constructor calls  

69
00:08:13,520 --> 00:08:20,480
receive as a parameter that "this" pointer of the 
derived class. So the base class  

70
00:08:20,480 --> 00:08:27,520
constructor would receive a "this" pointer. Then 
inside the constructor in this case the first  

71
00:08:27,520 --> 00:08:33,920
constructor code would be the Furniture so the 
constructor would fill all the relevant members  

72
00:08:33,920 --> 00:08:41,360
into this pointer and this is how we would get all 
the relevant members from the base classes to the  

73
00:08:41,360 --> 00:08:47,680
pointer to the derived class when we call the constructor. So in this  

74
00:08:47,680 --> 00:08:54,560
specific case another thing we can see after the 
constructor call, is that the offset from the  

75
00:08:54,560 --> 00:09:01,200
beginning of this would be incremented by 18 hex, 
this is because the size of the Furniture object  

76
00:09:01,200 --> 00:09:07,680
would be 0x18, and therefore when we are using 
and calling the second constructor we need to setup  

77
00:09:07,680 --> 00:09:15,920
the "this" pointers to the correct offset and 
and only then we would be able to call the second  

78
00:09:15,920 --> 00:09:23,520
constructor. So this is exactly what happened in 
here too. So, it sets the right offset into the  

79
00:09:24,640 --> 00:09:31,760
"this" pointer and then it pass this pointer into 
the Table constructor. Afterward we have a few  

80
00:09:31,760 --> 00:09:38,320
other things that happen. So, as we said previously 
the vtables are also main part when we are  

81
00:09:38,320 --> 00:09:45,280
dealing with multiple inheritance. So in this case 
we have the vtable for the Furniture CoffeeTable  

82
00:09:45,280 --> 00:09:52,400
and we also have the vtable of the Table 
CoffeeTable. Which means we are using two  

83
00:09:53,520 --> 00:10:01,360
vtables that are relevant for each 
base class. And those would be stored in the  

84
00:10:01,360 --> 00:10:07,120
correct offset for each one of them which means 
before the members of the relevant base class.  

85
00:10:07,120 --> 00:10:12,240
So, the first vtable would be in offset 0 
and the second one that is relevant for the Table  

86
00:10:12,240 --> 00:10:18,400
constructor would be in offset 0x18, and in the 
end of course you would also have to deal with  

87
00:10:18,400 --> 00:10:24,880
the members of our derived classes. In this case, it 
would be the shopId member that would be added  

88
00:10:24,880 --> 00:10:33,920
to the this pointer at offset 0x24. And this 
is it regarding the basics of multiple inheritance  

89
00:10:33,920 --> 00:10:40,080
In the next part we will also discuss how
virtual calls looks like in multiple inheritance  

90
00:10:40,080 --> 00:10:46,560
cases, what happens exactly when we need to 
call virtual functions but we actually have two  

91
00:10:46,560 --> 00:10:55,120
different vtables inside the object. So, I hope 
you learned so far and see you in the next video.
 

