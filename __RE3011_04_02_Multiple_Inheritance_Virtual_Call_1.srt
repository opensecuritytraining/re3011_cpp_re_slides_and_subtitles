1
00:00:00,320 --> 00:00:03,919
Hello!

2
00:00:01,360 --> 00:00:06,000
this video, today would focus on the

3
00:00:03,919 --> 00:00:07,359
Virtual Calls, in cases of multiple

4
00:00:06,000 --> 00:00:09,920
inheritance.

5
00:00:07,359 --> 00:00:11,759
I would like to begin with saying that

6
00:00:09,920 --> 00:00:14,320
if you haven't finished the exercises

7
00:00:11,759 --> 00:00:15,440
yet, please do so before continuing with

8
00:00:14,320 --> 00:00:17,440
the video.

9
00:00:15,440 --> 00:00:19,760
The video would

10
00:00:17,440 --> 00:00:23,279
include parts of

11
00:00:19,760 --> 00:00:24,560
structures and also have some clues on

12
00:00:23,279 --> 00:00:26,640
how to

13
00:00:24,560 --> 00:00:28,160
deal with the exercise that are

14
00:00:26,640 --> 00:00:29,439
relevant for the multiple

15
00:00:28,160 --> 00:00:31,279
inheritance

16
00:00:29,439 --> 00:00:34,239
struct creation

17
00:00:31,279 --> 00:00:36,640
and in order to get the best

18
00:00:34,239 --> 00:00:38,000
out of this i think 

19
00:00:36,640 --> 00:00:40,160
it would be better to finish the

20
00:00:38,000 --> 00:00:42,840
exercises first. So, if you haven't done

21
00:00:40,160 --> 00:00:47,680
that please pause and then continue

22
00:00:42,840 --> 00:00:50,559
later. And during this part, we would also

23
00:00:47,680 --> 00:00:51,440
take the example that we saw in previous

24
00:00:50,559 --> 00:00:54,320
parts

25
00:00:51,440 --> 00:00:56,480
and we would analyze two virtual calls

26
00:00:54,320 --> 00:00:58,480
we would do a static analysis of those

27
00:00:56,480 --> 00:01:01,440
virtual calls

28
00:00:58,480 --> 00:01:04,000
and as you may

29
00:01:01,440 --> 00:01:06,000
understand is that because we have

30
00:01:04,000 --> 00:01:10,000
multiple inheritance there would be more

31
00:01:06,000 --> 00:01:12,479
than one vtable. This means that in each

32
00:01:10,000 --> 00:01:14,400
virtual call would have to analyze the

33
00:01:12,479 --> 00:01:17,920
assembly to understand if what we are

34
00:01:14,400 --> 00:01:20,080
looking at is actually from

35
00:01:17,920 --> 00:01:22,320
which of the vtables and that the

36
00:01:20,080 --> 00:01:24,799
object has. So, because we have more than

37
00:01:22,320 --> 00:01:26,320
one, the virtual calls would have to be

38
00:01:24,799 --> 00:01:28,960
from one of them and we will have to

39
00:01:26,320 --> 00:01:32,400
understand it first before understanding

40
00:01:28,960 --> 00:01:35,600
the exact function that is used.

41
00:01:32,400 --> 00:01:38,159
So let's dive into an example

42
00:01:35,600 --> 00:01:40,240
and that will actually make the virtual

43
00:01:38,159 --> 00:01:41,439
calls of multiple inheritance much

44
00:01:40,240 --> 00:01:42,159
clearer.

45
00:01:41,439 --> 00:01:44,560
So,

46
00:01:42,159 --> 00:01:47,360
in this case we have the coffee table

47
00:01:44,560 --> 00:01:49,280
object again, that you are familiar with

48
00:01:47,360 --> 00:01:50,479
from the previous video.

49
00:01:49,280 --> 00:01:52,240
And

50
00:01:50,479 --> 00:01:55,200
in this case we would examine the

51
00:01:52,240 --> 00:01:58,399
assembly to understand the virtual calls.

52
00:01:55,200 --> 00:01:59,759
So, first what we would see is that the

53
00:01:58,399 --> 00:02:02,640
this pointer

54
00:01:59,759 --> 00:02:04,560
of the coffee table would be stored in

55
00:02:02,640 --> 00:02:06,560
var_18.

56
00:02:04,560 --> 00:02:08,000
And we can rename this variable so it

57
00:02:06,560 --> 00:02:09,200
would be easier to work with assembly.

58
00:02:08,000 --> 00:02:11,520

59
00:02:09,200 --> 00:02:13,520
So, this is what we did here. So, we

60
00:02:11,520 --> 00:02:14,480
renamed this variable to be

61
00:02:13,520 --> 00:02:16,400
“this”

62
00:02:14,480 --> 00:02:20,720
and now we know that what we are looking

63
00:02:16,400 --> 00:02:24,000
at would be the coffee table object.

64
00:02:20,720 --> 00:02:26,560
Also what we can see is that we have a

65
00:02:24,000 --> 00:02:29,360
two virtual calls here one of them is

66
00:02:26,560 --> 00:02:30,640
'call rdx' and the second one is 

67
00:02:29,360 --> 00:02:33,440
'call rax'.

68
00:02:30,640 --> 00:02:36,160
And we would examine first the 'call rax'

69
00:02:33,440 --> 00:02:38,800
to understand which vtable was used for

70
00:02:36,160 --> 00:02:41,680
this virtual call and in what offset to

71
00:02:38,800 --> 00:02:44,720
understand exactly what is the 

72
00:02:41,680 --> 00:02:48,640
function that was used in this case.

73
00:02:44,720 --> 00:02:51,280
So, this is what we will focus on now

74
00:02:48,640 --> 00:02:54,720
first we would have to understand which

75
00:02:51,280 --> 00:02:58,080
of the vtables will be used. So, we can

76
00:02:54,720 --> 00:03:00,959
see that because we're using the

77
00:02:58,080 --> 00:03:02,159
first offset so we will take the first 

78
00:03:00,959 --> 00:03:04,400
vtable

79
00:03:02,159 --> 00:03:08,480
from the object that is stored in offset 0

80
00:03:04,400 --> 00:03:10,720
and then we will dereference again and put

81
00:03:08,480 --> 00:03:13,760
a value in RDX and this would be the

82
00:03:10,720 --> 00:03:16,159
exact function that we want also in

83
00:03:13,760 --> 00:03:18,159
offset 0. So, it means we are taking

84
00:03:16,159 --> 00:03:20,159
the vtable in offset 0 and the

85
00:03:18,159 --> 00:03:21,280
function in offset 0 inside of

86
00:03:20,159 --> 00:03:23,680
vtable

87
00:03:21,280 --> 00:03:26,159
so in order to understand exactly which

88
00:03:23,680 --> 00:03:28,959
function and which vtable it is, we

89
00:03:26,159 --> 00:03:29,760
would have to analyze the object

90
00:03:28,959 --> 00:03:32,560
that

91
00:03:29,760 --> 00:03:35,200
we created in the constructor

92
00:03:32,560 --> 00:03:37,760
and to understand what is the

93
00:03:35,200 --> 00:03:40,080
exact function that was used for

94
00:03:37,760 --> 00:03:43,200
this virtual call.

95
00:03:40,080 --> 00:03:45,360
When we look at the coffee table object

96
00:03:43,200 --> 00:03:46,959
we can see that in the first offset we

97
00:03:45,360 --> 00:03:48,959
have the vtable

98
00:03:46,959 --> 00:03:51,760
and the object of

99
00:03:48,959 --> 00:03:54,560
the furniture. So, because coffee table

100
00:03:51,760 --> 00:03:55,760
inherits from furniture like we saw

101
00:03:54,560 --> 00:03:58,239
in previous

102
00:03:55,760 --> 00:04:00,159
parts of this of this class

103
00:03:58,239 --> 00:04:02,879
and we would have to examine the

104
00:04:00,159 --> 00:04:05,040
furniture object first to understand

105
00:04:02,879 --> 00:04:06,159
better about

106
00:04:05,040 --> 00:04:08,400
what is the

107
00:04:06,159 --> 00:04:09,840
exact function that was used in this

108
00:04:08,400 --> 00:04:12,480
case.

109
00:04:09,840 --> 00:04:14,720
So, if we look at the furniture

110
00:04:12,480 --> 00:04:16,400
object we can see of course that the

111
00:04:14,720 --> 00:04:18,239
vtable is in the

112
00:04:16,400 --> 00:04:20,880
first offset in 0

113
00:04:18,239 --> 00:04:23,440
and we can look in the assembly to

114
00:04:20,880 --> 00:04:25,199
analyze the vtable that was used and

115
00:04:23,440 --> 00:04:27,040
the exact function that was used in this

116
00:04:25,199 --> 00:04:30,400
case.

117
00:04:27,040 --> 00:04:34,560
So, if we go to the assembly and we look

118
00:04:30,400 --> 00:04:37,040
in the constructors we can see that the

119
00:04:34,560 --> 00:04:40,080
vtable that was used for the coffee

120
00:04:37,040 --> 00:04:43,600
table with the furniture 

121
00:04:40,080 --> 00:04:45,600
base class is the 'printPrice' so

122
00:04:43,600 --> 00:04:46,800
it would be in the first offset of the

123
00:04:45,600 --> 00:04:47,520
vtable.

124
00:04:46,800 --> 00:04:49,520
So,

125
00:04:47,520 --> 00:04:51,440
for RDX 

126
00:04:49,520 --> 00:04:54,400
in this case we would have the coffee

127
00:04:51,440 --> 00:04:57,040
table 'printPrice' function.

128
00:04:54,400 --> 00:04:59,840
So, the main part of

129
00:04:57,040 --> 00:05:03,199
what we needed to understand for the

130
00:04:59,840 --> 00:05:05,919
virtual call is the vtable. What is

131
00:05:03,199 --> 00:05:08,080
the vtable that was used for

132
00:05:05,919 --> 00:05:10,320
this case? Another thing that we need to

133
00:05:08,080 --> 00:05:13,360
understand is what is the exact offset

134
00:05:10,320 --> 00:05:18,000
of the function so this is what we

135
00:05:13,360 --> 00:05:19,600
saw from the RDX virtual call.

136
00:05:18,000 --> 00:05:22,800
And I will

137
00:05:19,600 --> 00:05:25,680
stop here to give you the chance to

138
00:05:22,800 --> 00:05:27,680
do the second virtual calls by yourself.

139
00:05:25,680 --> 00:05:28,720
So this virtual call

140
00:05:27,680 --> 00:05:31,680
is

141
00:05:28,720 --> 00:05:35,280
'call rax'. And because you already

142
00:05:31,680 --> 00:05:36,479
analyzed the same executable

143
00:05:35,280 --> 00:05:38,479
in

144
00:05:36,479 --> 00:05:41,280
the previous exercise.

145
00:05:38,479 --> 00:05:44,160
You can also go back to the same binary

146
00:05:41,280 --> 00:05:46,960
and examine and what you have there and

147
00:05:44,160 --> 00:05:48,639
see if you can analyze this virtual

148
00:05:46,960 --> 00:05:50,639
call by yourself.

149
00:05:48,639 --> 00:05:52,880
And after you finish you can continue

150
00:05:50,639 --> 00:05:54,880
with the video and then you would be

151
00:05:52,880 --> 00:05:58,000
able to

152
00:05:54,880 --> 00:06:01,440
have some short walkthrough on

153
00:05:58,000 --> 00:06:04,080
this virtual call analysis.