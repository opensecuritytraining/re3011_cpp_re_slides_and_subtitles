
154
00:00:00,000 --> 00:00:07,680
So after you

155
00:00:02,640 --> 00:00:09,120
did the second virtual call by yourself

156
00:00:06,240 --> 00:00:10,879
we would do this short walkthrough

157
00:00:09,120 --> 00:00:11,920
that i talked about

158
00:00:10,879 --> 00:00:15,120
and

159
00:00:11,920 --> 00:00:18,160
we would have this virtual call that’s

160
00:00:15,120 --> 00:00:20,080
'call rax' and we would analyze which

161
00:00:18,160 --> 00:00:23,599
vtable is being used which function

162
00:00:20,080 --> 00:00:25,280
exactly and also we would check

163
00:00:23,599 --> 00:00:28,879
if there is anything else that is

164
00:00:25,280 --> 00:00:32,800
special about this virtual call. 

165
00:00:28,879 --> 00:00:35,360
So, if we go backward and analyze what we

166
00:00:32,800 --> 00:00:37,920
see. So, we can see that "this" pointer is

167
00:00:35,360 --> 00:00:40,160
being stored in RAX

168
00:00:37,920 --> 00:00:41,280
and that we

169
00:00:40,160 --> 00:00:44,000

170
00:00:41,280 --> 00:00:48,080
later dereference the

171
00:00:44,000 --> 00:00:50,080
object that we have in offset 0x18.

172
00:00:48,080 --> 00:00:52,400
If you remember from

173
00:00:50,080 --> 00:00:54,240
the previous example and

174
00:00:52,400 --> 00:00:59,199
also if you're not i will of course show

175
00:00:54,240 --> 00:01:00,560
you. So, in offset 0x18, we will have the

176
00:00:59,199 --> 00:01:02,159
second

177
00:01:00,560 --> 00:01:02,960
inherited object.

178
00:01:02,159 --> 00:01:05,280
So,

179
00:01:02,960 --> 00:01:07,840
if we want to examine the

180
00:01:05,280 --> 00:01:10,640
structure of the coffee table again we

181
00:01:07,840 --> 00:01:12,560
can see that in offset 0x18. This is the

182
00:01:10,640 --> 00:01:15,840
beginning of the

183
00:01:12,560 --> 00:01:19,120
table object. So because the coffee table

184
00:01:15,840 --> 00:01:22,000
inherits from a two different object,

185
00:01:19,120 --> 00:01:24,479
so we would have first the parts that is

186
00:01:22,000 --> 00:01:27,280
relevant for the furniture and then we

187
00:01:24,479 --> 00:01:30,000
would have in the second part all the

188
00:01:27,280 --> 00:01:33,280
relevant members and vtable of the

189
00:01:30,000 --> 00:01:37,680
table which is the second inherited

190
00:01:33,280 --> 00:01:40,080
class. So in our case we have table.

191
00:01:37,680 --> 00:01:42,400
Like in previous examples what we would

192
00:01:40,080 --> 00:01:45,520
do is that we would have to analyze the

193
00:01:42,400 --> 00:01:47,439
table object to understand what we have

194
00:01:45,520 --> 00:01:49,439
in this offset exactly

195
00:01:47,439 --> 00:01:51,599
and we can assume it's the vtable again

196
00:01:49,439 --> 00:01:54,960
but 

197
00:01:51,599 --> 00:01:57,199
we will check it. When we look at the

198
00:01:54,960 --> 00:02:00,479
table object we can see that in the

199
00:01:57,199 --> 00:02:04,400
first offset, we have the vtable at

200
00:02:00,479 --> 00:02:07,039
offset 0 as we

201
00:02:04,400 --> 00:02:10,159
thought about and suggested before.

202
00:02:07,039 --> 00:02:13,360
But like we did in the previous

203
00:02:10,159 --> 00:02:16,400
virtual call we also need to understand

204
00:02:13,360 --> 00:02:18,159
what is the exact function that was

205
00:02:16,400 --> 00:02:21,439
used in this case.

206
00:02:18,159 --> 00:02:24,319
So, if we look at assembly again what we

207
00:02:21,439 --> 00:02:25,360
can see is that we are using the first

208
00:02:24,319 --> 00:02:27,840
offset

209
00:02:25,360 --> 00:02:29,680
inside this vtable. So we are the

210
00:02:27,840 --> 00:02:32,719
dereferencing 

211
00:02:29,680 --> 00:02:35,920
RAX with

212
00:02:32,719 --> 00:02:36,800
offset 0 and putting it in RAX

213
00:02:35,920 --> 00:02:38,959
again

214
00:02:36,800 --> 00:02:40,800
and this means that when we

215
00:02:38,959 --> 00:02:43,120
will call RAX

216
00:02:40,800 --> 00:02:45,920
it will call the

217
00:02:43,120 --> 00:02:48,959
function from the first offset of the

218
00:02:45,920 --> 00:02:50,320
Table's vtables.

219
00:02:48,959 --> 00:02:53,360
So, if

220
00:02:50,320 --> 00:02:55,839
we go back to the constructor of

221
00:02:53,360 --> 00:02:58,800
the coffee table and the table we would

222
00:02:55,839 --> 00:03:01,040
see that the vtable that is used

223
00:02:58,800 --> 00:03:05,519
inside the coffee table constructor

224
00:03:01,040 --> 00:03:06,959
would be this vtable that I show you here

225
00:03:05,519 --> 00:03:09,920
and

226
00:03:06,959 --> 00:03:12,240
in the first offset in offset 0

227
00:03:09,920 --> 00:03:14,720
we would have the function table set

228
00:03:12,240 --> 00:03:17,680
legs. So this is one of the functions

229
00:03:14,720 --> 00:03:18,640
that is part of the base class which is

230
00:03:17,680 --> 00:03:21,120
table,

231
00:03:18,640 --> 00:03:22,880
but of course

232
00:03:21,120 --> 00:03:25,440
we

233
00:03:22,880 --> 00:03:28,160
understood from an assembly so it's

234
00:03:25,440 --> 00:03:31,040
nice to see that even if we have

235
00:03:28,160 --> 00:03:34,399
two virtual calls from two different

236
00:03:31,040 --> 00:03:37,200
vtables the process of understanding

237
00:03:34,399 --> 00:03:39,600
which function it is and which vtable

238
00:03:37,200 --> 00:03:42,320
is used quite similar.

239
00:03:39,600 --> 00:03:43,200
So, now we know that the virtual call

240
00:03:42,320 --> 00:03:46,079
here

241
00:03:43,200 --> 00:03:49,200
would be 'Table::set_legs'.

242
00:03:46,079 --> 00:03:52,399
Another thing that you might notice

243
00:03:49,200 --> 00:03:54,880
is that we also have a parameter used

244
00:03:52,399 --> 00:03:56,320
for this function call. So because this

245
00:03:54,880 --> 00:03:58,320
is a function

246
00:03:56,320 --> 00:04:01,360
that receives an integer as the

247
00:03:58,320 --> 00:04:03,519
parameter, we would also have to set it

248
00:04:01,360 --> 00:04:07,120
in the assembly. So, in this case the

249
00:04:03,519 --> 00:04:10,079
parameter is set in ESI and is the

250
00:04:07,120 --> 00:04:11,680
number 3. And this number would be

251
00:04:10,079 --> 00:04:13,040
passed to the function

252
00:04:11,680 --> 00:04:15,680

253
00:04:13,040 --> 00:04:19,920
'set_legs' like you can see

254
00:04:15,680 --> 00:04:22,079
in this assembly vtable. After we

255
00:04:19,920 --> 00:04:24,320
finish the analysis of the virtual

256
00:04:22,079 --> 00:04:27,360
calls, and we can summarize what we

257
00:04:24,320 --> 00:04:30,959
learned so far and what we understood.

258
00:04:27,360 --> 00:04:33,519
So, if we want to summarize everything so

259
00:04:30,959 --> 00:04:36,800
creating the object structures in

260
00:04:33,519 --> 00:04:39,120
IDA is very important. This is

261
00:04:36,800 --> 00:04:40,240
a very important thing to understand

262
00:04:39,120 --> 00:04:42,639
because

263
00:04:40,240 --> 00:04:45,920
if you saw what we did during the

264
00:04:42,639 --> 00:04:48,240
virtual call analysis that was to go

265
00:04:45,920 --> 00:04:51,040
back to the structure of the relevant

266
00:04:48,240 --> 00:04:53,920
object analyze it again see which

267
00:04:51,040 --> 00:04:56,320
vtable was used from which

268
00:04:53,920 --> 00:04:59,360
of the base classes that it inherited

269
00:04:56,320 --> 00:05:01,280
from and if we would not create the

270
00:04:59,360 --> 00:05:02,399
structures, the process would take much

271
00:05:01,280 --> 00:05:03,920
longer.

272
00:05:02,399 --> 00:05:06,720
Don't think about like this specific

273
00:05:03,920 --> 00:05:09,680
example, but about bigger and more

274
00:05:06,720 --> 00:05:11,040
complex binaries. In those cases if you

275
00:05:09,680 --> 00:05:13,440
would not

276
00:05:11,040 --> 00:05:16,399
create the structures properly if you

277
00:05:13,440 --> 00:05:17,600
would not create everything around

278
00:05:16,399 --> 00:05:20,160
the objects

279
00:05:17,600 --> 00:05:22,399
it would be very hard to

280
00:05:20,160 --> 00:05:24,560
understand each time

281
00:05:22,399 --> 00:05:27,200
what is the exact

282
00:05:24,560 --> 00:05:28,480
virtual call that is used in there and it

283
00:05:27,200 --> 00:05:30,320
also

284
00:05:28,480 --> 00:05:32,399
would be very hard to understand the

285
00:05:30,320 --> 00:05:33,280
logic of the code.

286
00:05:32,399 --> 00:05:34,639
So

287
00:05:33,280 --> 00:05:37,120
I cannot

288
00:05:34,639 --> 00:05:40,959
explain to you how much time it

289
00:05:37,120 --> 00:05:43,200
takes, but also how profitable it can be

290
00:05:40,959 --> 00:05:45,440
if you do them so.

291
00:05:43,200 --> 00:05:46,880
This is like a

292
00:05:45,440 --> 00:05:48,240
tip from me

293
00:05:46,880 --> 00:05:49,600

294
00:05:48,240 --> 00:05:51,839
also

295
00:05:49,600 --> 00:05:54,320
another thing that we learned in this

296
00:05:51,839 --> 00:05:55,760
video is that, examining the object

297
00:05:54,320 --> 00:05:58,240
structure

298
00:05:55,760 --> 00:06:00,800
is very useful to understand the

299
00:05:58,240 --> 00:06:03,519
vtables and the virtual calls and in each

300
00:06:00,800 --> 00:06:06,800
virtual call, you would have to analyze

301
00:06:03,519 --> 00:06:09,280
the vtable that it was

302
00:06:06,800 --> 00:06:11,280
using and also the specific offset

303
00:06:09,280 --> 00:06:12,720
inside of a the vtable

304
00:06:11,280 --> 00:06:14,800
and this is

305
00:06:12,720 --> 00:06:16,880
besides getting the vtable, the process

306
00:06:14,800 --> 00:06:19,440
of understanding which function exactly

307
00:06:16,880 --> 00:06:21,760
is used, is very

308
00:06:19,440 --> 00:06:24,800
similar to the basic inheritance

309
00:06:21,760 --> 00:06:27,120
part. Another thing that we learned is

310
00:06:24,800 --> 00:06:30,800
that, when we do the static analysis

311
00:06:27,120 --> 00:06:32,880
after virtual calls, it takes time but

312
00:06:30,800 --> 00:06:34,079
it's actually

313
00:06:32,880 --> 00:06:36,399
quite

314
00:06:34,079 --> 00:06:39,760
repeatable after you understand the

315
00:06:36,399 --> 00:06:41,199
process. So, we go back analyze the vtable,

316
00:06:39,760 --> 00:06:43,600
analyze the

317
00:06:41,199 --> 00:06:45,920
offset inside of it and

318
00:06:43,600 --> 00:06:47,280
analyze mostly the assembly and do a lot

319
00:06:45,920 --> 00:06:48,240
of reversing.

320
00:06:47,280 --> 00:06:51,280
So

321
00:06:48,240 --> 00:06:53,680
this is in general what we've

322
00:06:51,280 --> 00:06:56,000
learned so far, and I hope you find it

323
00:06:53,680 --> 00:06:56,800
useful. And we will have another exercise

324
00:06:56,000 --> 00:06:59,760
now.

325
00:06:56,800 --> 00:06:59,760
Thank you!
 

