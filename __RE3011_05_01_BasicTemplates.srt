1
00:00:00,080 --> 00:00:04,720
Hello! Today our topic would be basic

2
00:00:03,199 --> 00:00:05,920
templates.

3
00:00:04,720 --> 00:00:08,960

4
00:00:05,920 --> 00:00:12,240
For the agenda we would discuss both

5
00:00:08,960 --> 00:00:14,799
how templates look like in binaries so

6
00:00:12,240 --> 00:00:16,800
we would try and understand how to

7
00:00:14,799 --> 00:00:17,920
identify templates what happens in

8
00:00:16,800 --> 00:00:21,600
binaries

9
00:00:17,920 --> 00:00:24,400
when we use templates and how we can

10
00:00:21,600 --> 00:00:26,960
also identify a functions that are

11
00:00:24,400 --> 00:00:28,800
templates and functions that are 

12
00:00:26,960 --> 00:00:31,279
just regular functions.

13
00:00:28,800 --> 00:00:33,920
So, for this

14
00:00:31,279 --> 00:00:36,719
explanation we will also use an example

15
00:00:33,920 --> 00:00:40,000
to show how different functions can be a

16
00:00:36,719 --> 00:00:41,040
template and also can be just regular

17
00:00:40,000 --> 00:00:43,360
functions.

18
00:00:41,040 --> 00:00:46,079
And we will also have a brief overview

19
00:00:43,360 --> 00:00:49,280
of how to define the basic template in

20
00:00:46,079 --> 00:00:51,120
IDA and there are

21
00:00:49,280 --> 00:00:53,840
more complex templates that we will not

22
00:00:51,120 --> 00:00:54,719
discuss during this part of the training.

23
00:00:53,840 --> 00:00:57,440

24
00:00:54,719 --> 00:01:00,719
For example objects

25
00:00:57,440 --> 00:01:03,199
as the template type and stuff like that

26
00:01:00,719 --> 00:01:05,040
but we will discuss the basic types

27
00:01:03,199 --> 00:01:07,200
of template and

28
00:01:05,040 --> 00:01:10,240
this would cover a lot of things you

29
00:01:07,200 --> 00:01:10,240
would see in binaries.

30
00:01:10,320 --> 00:01:14,640
So, regarding an intro,

31
00:01:12,560 --> 00:01:17,119
if you don't know what templates are so

32
00:01:14,640 --> 00:01:20,400
there are generic functions that can be

33
00:01:17,119 --> 00:01:22,640
used with different data types. So you

34
00:01:20,400 --> 00:01:23,759
can create a function that with

35
00:01:22,640 --> 00:01:24,960
different

36
00:01:23,759 --> 00:01:27,840
usages

37
00:01:24,960 --> 00:01:29,840
would have different types

38
00:01:27,840 --> 00:01:32,880
and the function itself would change a

39
00:01:29,840 --> 00:01:35,680
little based on the types that it uses

40
00:01:32,880 --> 00:01:37,920
and the compiler generates the variants

41
00:01:35,680 --> 00:01:40,000
of the same function

42
00:01:37,920 --> 00:01:41,759
as many times as required. So, it means

43
00:01:40,000 --> 00:01:44,320
that for each

44
00:01:41,759 --> 00:01:46,880
variant that we need each type that is

45
00:01:44,320 --> 00:01:49,119
used for the template there would be a

46
00:01:46,880 --> 00:01:51,439
different function in the binary. So for

47
00:01:49,119 --> 00:01:54,240
one template there could be many

48
00:01:51,439 --> 00:01:57,040
functions in the binaries and so it's

49
00:01:54,240 --> 00:01:58,320
actually kind of a macro in a way that

50
00:01:57,040 --> 00:02:00,880
just like creates more and more

51
00:01:58,320 --> 00:02:02,799
functions when it's compiled at the end.

52
00:02:00,880 --> 00:02:05,439
So you might

53
00:02:02,799 --> 00:02:07,680
already understand from that that when

54
00:02:05,439 --> 00:02:09,280
we do reversing this is something that

55
00:02:07,680 --> 00:02:12,800
takes a lot of time,

56
00:02:09,280 --> 00:02:15,680
and this is very complex. So

57
00:02:12,800 --> 00:02:16,560
like I said it requires a lot of time

58
00:02:15,680 --> 00:02:19,360
and

59
00:02:16,560 --> 00:02:21,200
you would have a lot of functions with

60
00:02:19,360 --> 00:02:23,280
similar functionality.

61
00:02:21,200 --> 00:02:25,599
But the types would be different. 

62
00:02:23,280 --> 00:02:27,599
For example the return value would

63
00:02:25,599 --> 00:02:29,760
be different, the parameters that are

64
00:02:27,599 --> 00:02:31,599
passed could be different,

65
00:02:29,760 --> 00:02:34,000
and the functionality inside would be

66
00:02:31,599 --> 00:02:36,480
similar but it would

67
00:02:34,000 --> 00:02:38,480
depend on the type. So this is something

68
00:02:36,480 --> 00:02:41,120
that you can identify

69
00:02:38,480 --> 00:02:43,840
and understand better about templates.

70
00:02:41,120 --> 00:02:45,840
Also define them in IDA is not super

71
00:02:43,840 --> 00:02:46,959
trivial, you would have to do a lot of

72
00:02:45,840 --> 00:02:49,040
adjustments

73
00:02:46,959 --> 00:02:50,800
and sometimes just like use the mangle

74
00:02:49,040 --> 00:02:52,480
names. So

75
00:02:50,800 --> 00:02:54,720
this is not very easy it's not like

76
00:02:52,480 --> 00:02:55,840
you have some just defined template and

77
00:02:54,720 --> 00:02:57,680
that's it.

78
00:02:55,840 --> 00:03:00,720
And also

79
00:02:57,680 --> 00:03:02,959
like if you have a non-basic type

80
00:03:00,720 --> 00:03:04,879
it would take more time it would be more

81
00:03:02,959 --> 00:03:06,640
complex to understand so you first would

82
00:03:04,879 --> 00:03:08,959
have to understand the objects and then

83
00:03:06,640 --> 00:03:10,000
continue and understanding the templates

84
00:03:08,959 --> 00:03:12,319
of the

85
00:03:10,000 --> 00:03:14,640
objects and the usages of it.

86
00:03:12,319 --> 00:03:17,360
So, for this topic we would not need to

87
00:03:14,640 --> 00:03:19,519
dive into those kind of scenarios but we

88
00:03:17,360 --> 00:03:22,000
will discuss the basic templates with

89
00:03:19,519 --> 00:03:24,080
the basic types.

90
00:03:22,000 --> 00:03:25,200
So in order to do that and to understand

91
00:03:24,080 --> 00:03:26,959
what we

92
00:03:25,200 --> 00:03:28,879
understood so far,

93
00:03:26,959 --> 00:03:31,040
we would use an example

94
00:03:28,879 --> 00:03:33,200
so in this example

95
00:03:31,040 --> 00:03:36,159
we can see there are three

96
00:03:33,200 --> 00:03:37,920
function calls that are used and we

97
00:03:36,159 --> 00:03:40,000
would examine the functions and the

98
00:03:37,920 --> 00:03:41,680
parameters that are used

99
00:03:40,000 --> 00:03:42,879
for each function to understand it

100
00:03:41,680 --> 00:03:43,680
better.

101
00:03:42,879 --> 00:03:47,280
So,

102
00:03:43,680 --> 00:03:50,239
first what we would do we would define

103
00:03:47,280 --> 00:03:53,599
the different local variables in the

104
00:03:50,239 --> 00:03:54,799
binary. So we have both integers and a

105
00:03:53,599 --> 00:03:57,280
long type.

106
00:03:54,799 --> 00:03:58,080
So, I changed the binary as you can see

107
00:03:57,280 --> 00:04:00,560
here

108
00:03:58,080 --> 00:04:02,879
and the parameters that

109
00:04:00,560 --> 00:04:05,360
are relevant and the local variables

110
00:04:02,879 --> 00:04:07,920
that are relevant are marked as integer

111
00:04:05,360 --> 00:04:10,640
or longs and let's continue and

112
00:04:07,920 --> 00:04:12,560
understand better which one relevant to

113
00:04:10,640 --> 00:04:14,959
a which function.

114
00:04:12,560 --> 00:04:16,000
If we start with examining the first

115
00:04:14,959 --> 00:04:18,560
function,

116
00:04:16,000 --> 00:04:22,079
so what we can see is that we have two

117
00:04:18,560 --> 00:04:25,120
numbers both of them of type int,

118
00:04:22,079 --> 00:04:27,680
5 and 6 in this case specifically. But we can

119
00:04:25,120 --> 00:04:31,120
see that because we are using edx and

120
00:04:27,680 --> 00:04:32,080
eax, which are four bytes registers. 

121
00:04:31,120 --> 00:04:34,400
The

122
00:04:32,080 --> 00:04:36,960
parameters that are used are actually

123
00:04:34,400 --> 00:04:39,040
integers and not a long

124
00:04:36,960 --> 00:04:41,600
and we also can see that it's not a

125
00:04:39,040 --> 00:04:42,720
float type because we are not using any

126
00:04:41,600 --> 00:04:45,360
of the

127
00:04:42,720 --> 00:04:46,320
float opcodes, this is just like a

128
00:04:45,360 --> 00:04:49,520
move

129
00:04:46,320 --> 00:04:52,000
and a call and nothing that

130
00:04:49,520 --> 00:04:54,400
call it with a float numbers.

131
00:04:52,000 --> 00:04:56,639
So, in this case it will be int_a and int_b

132
00:04:54,400 --> 00:04:59,280

133
00:04:56,639 --> 00:05:02,240
If we examine the second

134
00:04:59,280 --> 00:05:04,720
function, so this function receives two

135
00:05:02,240 --> 00:05:07,440
numbers but in this time it will be

136
00:05:04,720 --> 00:05:09,120
eight bytes and so in this case we can

137
00:05:07,440 --> 00:05:13,440
see that the

138
00:05:09,120 --> 00:05:16,400
numbers are stored in RDX and RAX both of

139
00:05:13,440 --> 00:05:19,440
them are eight bytes, and in this case it

140
00:05:16,400 --> 00:05:21,199
will be two longs. So, it will be long_a

141
00:05:19,440 --> 00:05:23,759
and long_b.

142
00:05:21,199 --> 00:05:24,639
And the numbers for them would be 5 and

143
00:05:23,759 --> 00:05:27,520
10.

144
00:05:24,639 --> 00:05:30,880
You can see because it's like a HEX.

145
00:05:27,520 --> 00:05:32,880
So, the number 10 in decimal.

146
00:05:30,880 --> 00:05:35,199
And for the

147
00:05:32,880 --> 00:05:38,160
last and the third function

148
00:05:35,199 --> 00:05:40,400
we can see that we have two integers too,

149
00:05:38,160 --> 00:05:41,840
so in this case it will be the number

150
00:05:40,400 --> 00:05:44,479
four and nine

151
00:05:41,840 --> 00:05:47,840
and both of them are also stored in EDX

152
00:05:44,479 --> 00:05:50,560
and EAX, and those are four bytes

153
00:05:47,840 --> 00:05:50,560
registers.

154
00:05:50,880 --> 00:05:54,720
So, in order to

155
00:05:52,639 --> 00:05:57,039
continue with our research, we would also

156
00:05:54,720 --> 00:05:59,360
have to examine the functions themselves.

157
00:05:57,039 --> 00:06:01,280
So, if we want to do so we would have to

158
00:05:59,360 --> 00:06:03,600
rename everything so it will be much

159
00:06:01,280 --> 00:06:05,759
easier to understand what

160
00:06:03,600 --> 00:06:07,840
of the functions, we are talking about. So

161
00:06:05,759 --> 00:06:10,400
the first function with the two integers

162
00:06:07,840 --> 00:06:12,639
would be called function A

163
00:06:10,400 --> 00:06:15,039
the second one with the two long

164
00:06:12,639 --> 00:06:17,039
will be a function B,

165
00:06:15,039 --> 00:06:19,120
and the third one with the two integers

166
00:06:17,039 --> 00:06:21,520
would be a function C.

167
00:06:19,120 --> 00:06:24,080
So, now we will dive into each one of

168
00:06:21,520 --> 00:06:25,919
them to understand better what is the

169
00:06:24,080 --> 00:06:29,280
purpose of each function

170
00:06:25,919 --> 00:06:31,600
and what is exactly the purpose of it

171
00:06:29,280 --> 00:06:35,440
and we will try to correlate it with

172
00:06:31,600 --> 00:06:38,479
what we learned how about templates.

173
00:06:35,440 --> 00:06:40,160
So, first let's examine function a

174
00:06:38,479 --> 00:06:41,280
so this is the first function we are

175
00:06:40,160 --> 00:06:43,360
examining.

176
00:06:41,280 --> 00:06:45,440
We would change first the function

177
00:06:43,360 --> 00:06:47,440
definition the declaration,

178
00:06:45,440 --> 00:06:50,800
and we would change it to have two

179
00:06:47,440 --> 00:06:54,960
parameters of type it. So, as you can see

180
00:06:50,800 --> 00:06:58,160
here I changed it to int a, and int b.

181
00:06:54,960 --> 00:06:59,840
The return value is still unclear

182
00:06:58,160 --> 00:07:03,039
because we haven't reversed the function

183
00:06:59,840 --> 00:07:05,919
yet, so we would continue research it and

184
00:07:03,039 --> 00:07:08,479
afterwards we will change it to whatever

185
00:07:05,919 --> 00:07:09,680
is the most suitable type for

186
00:07:08,479 --> 00:07:12,240
it.

187
00:07:09,680 --> 00:07:14,400
So, after we finish the declaration we

188
00:07:12,240 --> 00:07:16,880
can dive into the assembly itself

189
00:07:14,400 --> 00:07:18,880
and you can see that the parameters that

190
00:07:16,880 --> 00:07:21,039
we passed int A and B.

191
00:07:18,880 --> 00:07:24,080
Are stored locally on the stack,

192
00:07:21,039 --> 00:07:27,759
so we can rename the local variables

193
00:07:24,080 --> 00:07:29,599
so I change int A and int B.

194
00:07:27,759 --> 00:07:32,720
And afterwards we can see that the

195
00:07:29,599 --> 00:07:34,160
numbers are compared,

196
00:07:32,720 --> 00:07:36,319
and

197
00:07:34,160 --> 00:07:38,880
after they are compared they check if

198
00:07:36,319 --> 00:07:41,440
which one of the numbers is

199
00:07:38,880 --> 00:07:42,639
larger than the other. So, if B is larger

200
00:07:41,440 --> 00:07:46,400
than A

201
00:07:42,639 --> 00:07:49,360
so the function will return B.

202
00:07:46,400 --> 00:07:52,000
But, if A is larger than B, so we would

203
00:07:49,360 --> 00:07:54,560
return the

204
00:07:52,000 --> 00:07:55,360
A number.

205
00:07:54,560 --> 00:07:58,479
So,

206
00:07:55,360 --> 00:08:00,800
what we can see here is that the

207
00:07:58,479 --> 00:08:01,759
function check which of the numbers is

208
00:08:00,800 --> 00:08:02,639
larger

209
00:08:01,759 --> 00:08:04,960
and

210
00:08:02,639 --> 00:08:07,599
returns the larger number out of the two

211
00:08:04,960 --> 00:08:09,599
that we provided, in this case

212
00:08:07,599 --> 00:08:10,840
both of them are integers so it will

213
00:08:09,599 --> 00:08:14,960
return an

214
00:08:10,840 --> 00:08:16,639
integer and because we know now that the

215
00:08:14,960 --> 00:08:19,280
return value is an integer we can

216
00:08:16,639 --> 00:08:20,800
also change the function declaration.

217
00:08:19,280 --> 00:08:22,720
So, now we change the function

218
00:08:20,800 --> 00:08:24,479
declaration and

219
00:08:22,720 --> 00:08:25,840
we also change the function A

220
00:08:24,479 --> 00:08:26,800
because now we know what is this

221
00:08:25,840 --> 00:08:29,199
function

222
00:08:26,800 --> 00:08:31,840
exactly. And after we finish with

223
00:08:29,199 --> 00:08:33,839
function A. And we understood how it

224
00:08:31,840 --> 00:08:36,880
looks like and what are the exact types

225
00:08:33,839 --> 00:08:38,959
that it receives. We can go and overview

226
00:08:36,880 --> 00:08:41,680
a function B.

227
00:08:38,959 --> 00:08:44,240
So, because it's and the process will be

228
00:08:41,680 --> 00:08:46,240
faster this time because we already

229
00:08:44,240 --> 00:08:47,279
saw function A, function B is quite

230
00:08:46,240 --> 00:08:50,320
similar.

231
00:08:47,279 --> 00:08:53,279
So, we receive both of the long numbers

232
00:08:50,320 --> 00:08:54,480
in RDI and RSI and they are stored on

233
00:08:53,279 --> 00:08:56,720
the stack.

234
00:08:54,480 --> 00:08:59,040
So, we change again the

235
00:08:56,720 --> 00:09:00,160
name of the local variables to long_a

236
00:08:59,040 --> 00:09:02,240
and b.

237
00:09:00,160 --> 00:09:04,959
And we can see that later in this

238
00:09:02,240 --> 00:09:07,440
function we are having a comparison

239
00:09:04,959 --> 00:09:08,560
between the two numbers so they are both

240
00:09:07,440 --> 00:09:11,920
compared.

241
00:09:08,560 --> 00:09:14,240
And afterwards if a long_b is larger

242
00:09:11,920 --> 00:09:15,360
than long_a, so the function

243
00:09:14,240 --> 00:09:17,120
returns

244
00:09:15,360 --> 00:09:18,640
the long_b.

245
00:09:17,120 --> 00:09:22,720
And if

246
00:09:18,640 --> 00:09:25,200
A is larger than B, so it will return A.

247
00:09:22,720 --> 00:09:27,200
You might feel like you've already saw

248
00:09:25,200 --> 00:09:29,360
some similar functions, so

249
00:09:27,200 --> 00:09:30,720
as you assume this is very similar to

250
00:09:29,360 --> 00:09:32,880
function A.

251
00:09:30,720 --> 00:09:35,519
The function checks which one of the

252
00:09:32,880 --> 00:09:38,080
numbers is larger and then returns the

253
00:09:35,519 --> 00:09:41,839
larger number, and the return value is a

254
00:09:38,080 --> 00:09:41,839
long this time and not an int.

255
00:09:42,399 --> 00:09:47,839
for the third function, so function C.

256
00:09:45,440 --> 00:09:50,959
We can see that the function is much

257
00:09:47,839 --> 00:09:53,920
shorter, just a few lines of assembly we

258
00:09:50,959 --> 00:09:56,480
have the two received parameters which

259
00:09:53,920 --> 00:09:58,720
is int_d and int_c.

260
00:09:56,480 --> 00:10:00,880
Both of them are all stored on the stack

261
00:09:58,720 --> 00:10:03,440
like in the previous cases,

262
00:10:00,880 --> 00:10:05,839
just this time the function subtracts

263
00:10:03,440 --> 00:10:09,760
the two numbers from each other. So we

264
00:10:05,839 --> 00:10:11,839
have int D and it just subtracts int C from

265
00:10:09,760 --> 00:10:14,720
it and returned the result.

266
00:10:11,839 --> 00:10:16,959
So, this function is different although

267
00:10:14,720 --> 00:10:18,720
it receives the same parameters.

268
00:10:16,959 --> 00:10:21,040
So, if you want to conclude everything

269
00:10:18,720 --> 00:10:23,600
that we've learned so far and understood.

270
00:10:21,040 --> 00:10:26,640
So, we have function a and b both of them

271
00:10:23,600 --> 00:10:29,519
calculate the maximum of two numbers. And

272
00:10:26,640 --> 00:10:32,399
returns the number that is larger.

273
00:10:29,519 --> 00:10:33,519
Function A uses two integers and

274
00:10:32,399 --> 00:10:35,519
function B

275
00:10:33,519 --> 00:10:37,279
uses two longs.

276
00:10:35,519 --> 00:10:39,440
and function C is a little bit different,

277
00:10:37,279 --> 00:10:42,000
it subtract two numbers

278
00:10:39,440 --> 00:10:43,120
and both of the numbers need to be

279
00:10:42,000 --> 00:10:45,519
integers.

280
00:10:43,120 --> 00:10:47,360
And if we want to understand and based

281
00:10:45,519 --> 00:10:50,079
on what we learned so far

282
00:10:47,360 --> 00:10:52,320
we can see that actually a function a

283
00:10:50,079 --> 00:10:54,720
and b are part of a template because

284
00:10:52,320 --> 00:10:57,519
both of them have the same purpose,

285
00:10:54,720 --> 00:10:59,839
they have the same a function but they

286
00:10:57,519 --> 00:11:01,920
are using different types and return

287
00:10:59,839 --> 00:11:04,640
different types but both of them

288
00:11:01,920 --> 00:11:07,120
calculate which of the numbers is larger

289
00:11:04,640 --> 00:11:09,920
and returns the larger number.

290
00:11:07,120 --> 00:11:10,880
This is why this is a template.

291
00:11:09,920 --> 00:11:13,200
So,

292
00:11:10,880 --> 00:11:15,519
if we look at function C, we can see it's

293
00:11:13,200 --> 00:11:18,399
different. It's just like a regular

294
00:11:15,519 --> 00:11:20,640
function that subtract two numbers.

295
00:11:18,399 --> 00:11:22,720
What I'm trying to emphasize here is

296
00:11:20,640 --> 00:11:24,320
that, in order to understand that the

297
00:11:22,720 --> 00:11:27,040
function that you are looking at is

298
00:11:24,320 --> 00:11:29,680
actually a template. You would have to

299
00:11:27,040 --> 00:11:32,000
research reverse and analyze a lot of

300
00:11:29,680 --> 00:11:34,160
the binary and a lot of the functions to

301
00:11:32,000 --> 00:11:34,959
understand those correlations.

302
00:11:34,160 --> 00:11:37,200
So,

303
00:11:34,959 --> 00:11:38,240
in this case it's quite a short

304
00:11:37,200 --> 00:11:41,040
functions

305
00:11:38,240 --> 00:11:43,120
only a few lines of assembly code but

306
00:11:41,040 --> 00:11:45,040
if you have a larger binary you would

307
00:11:43,120 --> 00:11:48,079
have to do a lot of

308
00:11:45,040 --> 00:11:50,160
reversing work in order to

309
00:11:48,079 --> 00:11:53,440
understand the purpose of each of the

310
00:11:50,160 --> 00:11:54,320
functions and to understand the different

311
00:11:53,440 --> 00:11:56,639
types.

312
00:11:54,320 --> 00:11:58,880
So, you would have to understand what are

313
00:11:56,639 --> 00:12:01,200
the different types a function receives

314
00:11:58,880 --> 00:12:03,680
what it returns and also which of the

315
00:12:01,200 --> 00:12:06,560
function actually do the same thing just

316
00:12:03,680 --> 00:12:08,800
with different types, and in this way you

317
00:12:06,560 --> 00:12:11,519
would be able to correlate

318
00:12:08,800 --> 00:12:13,920
from the functions that you reverse and

319
00:12:11,519 --> 00:12:16,000
research and what exactly are the

320
00:12:13,920 --> 00:12:18,000
templates that you have in the binary.

321
00:12:16,000 --> 00:12:20,079
But as you can see in this case you also

322
00:12:18,000 --> 00:12:21,760
have regular functions that are just

323
00:12:20,079 --> 00:12:24,480
like defined

324
00:12:21,760 --> 00:12:26,079
like in a very low way.

325
00:12:24,480 --> 00:12:28,320
And you would have to do a lot of work

326
00:12:26,079 --> 00:12:30,560
to get it, so this is why i said like

327
00:12:28,320 --> 00:12:32,959
reversing template is a very

328
00:12:30,560 --> 00:12:34,880
complex thing to do when you do

329
00:12:32,959 --> 00:12:37,600
reversing. So

330
00:12:34,880 --> 00:12:40,240
this requires a lot of work but

331
00:12:37,600 --> 00:12:42,720
after you do all of those things, so the

332
00:12:40,240 --> 00:12:44,320
binary kind of have a whole map of the

333
00:12:42,720 --> 00:12:46,160
functions and you would have a better

334
00:12:44,320 --> 00:12:50,320
understanding of the binary that you are

335
00:12:46,160 --> 00:12:52,240
researching. So, this is very important.

336
00:12:50,320 --> 00:12:54,560
Another thing that we would have to

337
00:12:52,240 --> 00:12:57,120
cover, because it's very important, is

338
00:12:54,560 --> 00:12:59,680
defining the templates in IDA. You saw

339
00:12:57,120 --> 00:13:02,320
previously, I just like changed the names

340
00:12:59,680 --> 00:13:04,800
and the declaration only the parameters

341
00:13:02,320 --> 00:13:06,880
and the return value were changed. But in

342
00:13:04,800 --> 00:13:09,760
this case we will have a template and we

343
00:13:06,880 --> 00:13:11,680
do want to use for example the same name

344
00:13:09,760 --> 00:13:14,560
but with different declaration because

345
00:13:11,680 --> 00:13:15,440
the declaration should have the template

346
00:13:14,560 --> 00:13:17,440
kind of

347
00:13:15,440 --> 00:13:19,839
convention.

348
00:13:17,440 --> 00:13:21,839
But, in order to do it in IDA, it's not

349
00:13:19,839 --> 00:13:24,560
like you have a button to just click say

350
00:13:21,839 --> 00:13:27,040
“okay” this is template that's it. Bye. You

351
00:13:24,560 --> 00:13:29,839
would have to do a lot of

352
00:13:27,040 --> 00:13:31,360
mapping and to understand how to do

353
00:13:29,839 --> 00:13:34,000
it, you would have to

354
00:13:31,360 --> 00:13:35,440
understand the mangled name of the

355
00:13:34,000 --> 00:13:36,240
function you want.

356
00:13:35,440 --> 00:13:38,880
So,

357
00:13:36,240 --> 00:13:40,320
for example if we want to take the long

358
00:13:38,880 --> 00:13:42,160
max long long,

359
00:13:40,320 --> 00:13:44,320
which is one of the functions that we

360
00:13:42,160 --> 00:13:46,160
researched. We would have to use the 

361
00:13:44,320 --> 00:13:48,639
mangled name. In this case you can see it's

362
00:13:46,160 --> 00:13:51,440
here on the slide but you would have to

363
00:13:48,639 --> 00:13:53,600
use the mangled name in order to

364
00:13:51,440 --> 00:13:55,600
change the function name to be

365
00:13:53,600 --> 00:13:58,240
more

366
00:13:55,600 --> 00:14:00,160
specific and more correct.

367
00:13:58,240 --> 00:14:02,959
So, in order to do that you can right

368
00:14:00,160 --> 00:14:04,240
click and edit function or you can use

369
00:14:02,959 --> 00:14:07,440
alt+P.

370
00:14:04,240 --> 00:14:09,440
And then you would have this window open,

371
00:14:07,440 --> 00:14:12,160
you can use the name of function to

372
00:14:09,440 --> 00:14:14,240
change the name. If you just use N it's

373
00:14:12,160 --> 00:14:17,279
not going to be good enough in many of the

374
00:14:14,240 --> 00:14:19,360
cases. So this is giving you a more

375
00:14:17,279 --> 00:14:20,560
complete understanding of what you have

376
00:14:19,360 --> 00:14:23,120
in the function,

377
00:14:20,560 --> 00:14:25,680
it can also help you in other cases and

378
00:14:23,120 --> 00:14:28,959
when you do reversing. But, if we use that

379
00:14:25,680 --> 00:14:31,440
so just take the mangled name

380
00:14:28,959 --> 00:14:34,480
of the function and we can write it in

381
00:14:31,440 --> 00:14:34,480
the name of the function.

382
00:14:34,560 --> 00:14:38,399
Now, after we do that

383
00:14:36,240 --> 00:14:39,839
we can see that the binary and the

384
00:14:38,399 --> 00:14:42,480
name of the function

385
00:14:39,839 --> 00:14:46,160
were changed from the

386
00:14:42,480 --> 00:14:48,000
long mark long two long marks with this

387
00:14:46,160 --> 00:14:50,639
and

388
00:14:48,000 --> 00:14:53,199
brackets are on the long.

389
00:14:50,639 --> 00:14:55,360
And it's more clear that what we are

390
00:14:53,199 --> 00:14:57,920
looking at is actually a template, we can

391
00:14:55,360 --> 00:14:58,959
do the same for the int type of this

392
00:14:57,920 --> 00:15:01,040
function

393
00:14:58,959 --> 00:15:02,720
and in other cases you can do the same

394
00:15:01,040 --> 00:15:04,399
for each of the functions.

395
00:15:02,720 --> 00:15:06,480
And the only limitations that you would

396
00:15:04,399 --> 00:15:08,800
have to have the mangled names for each

397
00:15:06,480 --> 00:15:10,079
of the functions, you can just like

398
00:15:08,800 --> 00:15:13,040
analyze

399
00:15:10,079 --> 00:15:15,040
the function that you want to rename

400
00:15:13,040 --> 00:15:16,000
to create a mangled name for each of

401
00:15:15,040 --> 00:15:18,639
those.

402
00:15:16,000 --> 00:15:20,399
So, and this is what you

403
00:15:18,639 --> 00:15:22,240
actually have to do when you have basic

404
00:15:20,399 --> 00:15:24,320
templates in the binary

405
00:15:22,240 --> 00:15:26,480
and you would have to research

406
00:15:24,320 --> 00:15:28,880
everything, understand their functions,

407
00:15:26,480 --> 00:15:29,920
understand which function do a similar

408
00:15:28,880 --> 00:15:31,360
purpose

409
00:15:29,920 --> 00:15:33,440
and which

410
00:15:31,360 --> 00:15:36,240
parameters are different to create the

411
00:15:33,440 --> 00:15:37,759
template and to understand how to rename

412
00:15:36,240 --> 00:15:42,000
and understand that.

413
00:15:37,759 --> 00:15:44,399
It's not like a very easy and

414
00:15:42,000 --> 00:15:46,240
short work, it will take time but it will

415
00:15:44,399 --> 00:15:48,240
be worth it and your binary will look

416
00:15:46,240 --> 00:15:50,240
much much better.

417
00:15:48,240 --> 00:15:52,639
So, I hope you learn from it. And good

418
00:15:50,240 --> 00:15:52,639
luck!!
 

